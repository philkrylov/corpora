from django.utils.safestring import mark_safe

from rest_framework.serializers import ModelSerializer, BooleanField, CharField

from .models import (
    AffixFeature,
    Dialect,
    DictionaryEntry,
    Genre,
    Person,
    Place,
    Text,
    TextSentence,
    TextWord,
    WordFeature,
    Wordform,
    WordGrammar,
    WordGrammarFeature,
)


def _highlight(text, needle):
    return mark_safe(text.replace(needle,
                                  '<span class="highlight">%s</span>' % needle)
                     if needle else text)


def _remove_nulls(d):
    for k, v in list(d.items()):
        if v is None:
            del d[k]
        elif isinstance(v, dict):
            _remove_nulls(v)
            if not v:
                del d[k]
        elif isinstance(v, list):
            if not v:
                del d[k]
            else:
                for t in v:
                    if isinstance(t, dict):
                        _remove_nulls(t)


class HighlightedCharField(CharField):
    def to_representation(self, value):
        ru = self.context['request'].GET.get('translation_ru')
        if ru:
            return _highlight(value, ru)
        return value


class NonNullSerializer(ModelSerializer):
    def to_representation(self, instance):
        ret = super(NonNullSerializer, self).to_representation(instance)
        _remove_nulls(ret)
        return ret


class DictionaryEntrySerializer(NonNullSerializer):
    class Meta:
        model = DictionaryEntry
        fields = ('lexeme', 'lexeme_no', 'raw_stem', 'semgloss')


class AffixFeatureSerializer(NonNullSerializer):
    class Meta:
        model = AffixFeature
        fields = ('signature',)


class WordFeatureSerializer(NonNullSerializer):
    feature = AffixFeatureSerializer()

    class Meta:
        model = WordFeature
        fields = ('affix', 'feature')


class WordGrammarFeatureSerializer(NonNullSerializer):
    word_feature = WordFeatureSerializer()

    class Meta:
        model = WordGrammarFeature
        fields = ('position', 'word_feature')


class WordGrammarSerializer(NonNullSerializer):
    lexeme = DictionaryEntrySerializer()
    features = WordGrammarFeatureSerializer(many=True)

    class Meta:
        model = WordGrammar
        fields = ('lexeme', 'features')


class WordformSerializer(NonNullSerializer):
    grammars = WordGrammarSerializer(many=True)

    class Meta:
        model = Wordform
        fields = '__all__'


class TextWordSerializer(NonNullSerializer):
    wordform = WordformSerializer()
    is_match = BooleanField()

    class Meta:
        model = TextWord
        fields = ('number', 'leftpunct', 'wordform', 'rightpunct',
                  'confirmed_grammar', 'is_match')


class TextSentenceSerializer(NonNullSerializer):
    words = TextWordSerializer(many=True)
    translation_ru = HighlightedCharField()

    class Meta:
        model = TextSentence
        fields = '__all__'


class DialectSerializer(NonNullSerializer):
    class Meta:
        model = Dialect
        fields = '__all__'


class PlaceSerializer(NonNullSerializer):
    class Meta:
        model = Place
        fields = '__all__'


class PersonSerializer(NonNullSerializer):
    birth_place = PlaceSerializer()

    class Meta:
        model = Person
        fields = '__all__'


class GenreSerializer(NonNullSerializer):
    class Meta:
        model = Genre
        fields = '__all__'


class TextSerializer(NonNullSerializer):
    author = PersonSerializer()
    dialect = DialectSerializer()
    storyteller = PersonSerializer()
    editor = PersonSerializer()
    genre = GenreSerializer()
    record_place = PlaceSerializer()

    class Meta:
        model = Text
        fields = '__all__'
