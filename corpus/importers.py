from functools import partial
import logging
import re

from django.conf import settings
from django.db import transaction
from django.db.models import Q
from django.utils import timezone

from .import_aares import aares_paragraph_iterator
from .import_docx import docx_paragraph_iterator
from .import_xml import xml_paragraph_iterator
from .models import (
    AffixFeature,
    DictionaryEntry,
    TextSentence,
    TextWord,
    WordFeature,
    WordGrammar,
    WordGrammarFeature,
    Wordform,
)
from .tokenizer import split_punct


IMPORTERS = dict(
    aares=aares_paragraph_iterator,
    xml=xml_paragraph_iterator,
    docx=docx_paragraph_iterator,
    docx_p=partial(docx_paragraph_iterator, phonetic=True),
    docx_3=partial(docx_paragraph_iterator, refs=True),
    docx_4=partial(docx_paragraph_iterator, refs=True, phonetic=True),
)

logger = logging.getLogger(__name__)


def import_from_iterator(text, iterator, normalize, is_alphabetic):
    n_phrases = 0
    n_word_tokens = 0
    n_alpha_words = 0

    logger.debug("dropping existing sentences: %s",
                 TextSentence.objects.filter(text=text).delete())
    stale_grammars = WordGrammar.objects \
        .filter(auto=False) \
        .exclude(id__in=TextWord.objects
                 .filter(confirmed_grammar_id__isnull=False)
                 .values_list('confirmed_grammar_id', flat=True)
                 .distinct())
    logger.info("dropping stale manual grammars: %s", stale_grammars.delete())
    '''
    stale_wordforms = Wordform.objects \
        .exclude(id__in=(TextWord.objects.all()
                         .values_list('wordform_id', flat=True)
                         .distinct())) \
        .exclude(id__in=(WordGrammar.objects.filter(auto=False)
                         .values_list('wordform_id', flat=True)
                         .distinct()))
    logger.info("dropping stale wordforms: %s", stale_wordforms.delete())
    '''

    n_para = 1
    for paragraph in iterator:
        n_sent = 1
        for sent_props, words in paragraph:
            with transaction.atomic():
                sent = TextSentence.objects.create(paragraph_number=n_para,
                                                   text=text,
                                                   number=n_sent,
                                                   **sent_props)
                n_word = 1
                n_clause = 1
                for word, morphemes in words:
                    leftpunct, word, rightpunct = split_punct(word)
                    wordform, created = Wordform.objects \
                        .get_or_create(wordform=normalize(word, lower=False))
                    textword = TextWord(sentence=sent,
                                        number=n_word,
                                        clause=n_clause,
                                        leftpunct=leftpunct,
                                        wordform=wordform,
                                        rightpunct=rightpunct)
                    if morphemes:
                        try:
                            stem_txt, stem_gls = next(morphemes)
                        except StopIteration:
                            pass
                        else:
                            grammar_extra = {}
                            try:
                                entry = DictionaryEntry.objects \
                                    .get(Q(lexeme=stem_txt) |
                                         Q(raw_stem=stem_txt + '-'),
                                         semgloss=stem_gls)
                            except Exception:
                                entry = None
                                grammar_extra = {'raw_stem': stem_txt,
                                                 'raw_semgloss': stem_gls}
                            key = str(entry)
                            wgfs = []
                            n_pos = 1
                            for stem_txt, stem_gls in morphemes:
                                feat, created = AffixFeature.objects \
                                    .get_or_create(
                                        signature__iexact=stem_gls,
                                        defaults={'signature': stem_gls},
                                    )
                                wordfeat, created = WordFeature.objects \
                                    .get_or_create(affix=stem_txt,
                                                   feature=feat)
                                key += '+%d:%s.%s' % (n_pos, stem_txt,
                                                      stem_gls.upper())
                                wgfs.append([n_pos, wordfeat])
                            grammar, created = WordGrammar.objects \
                                .get_or_create(wordform=wordform,
                                               auto=False,
                                               lexeme=entry,
                                               key=key,
                                               weight=len(wgfs),
                                               **grammar_extra)
                            if created:
                                for n_pos, wordfeat in wgfs:
                                    WordGrammarFeature.objects \
                                        .create(word_grammar=grammar,
                                                position=n_pos,
                                                word_feature=wordfeat)
                            textword.confirmed_grammar = grammar
                    textword.save()
                    n_word_tokens += 1
                    n_word += 1
                    if rightpunct in (',', ';'):
                        n_clause += 1
                    if is_alphabetic(word):
                        n_alpha_words += 1
            n_phrases += 1
            n_sent += 1
        n_para += 1
    if n_para == 1:
        logger.error("No paragraphs in iterator")
    text.n_word_tokens = n_word_tokens
    text.n_phrases = n_phrases
    text.n_alpha_words = n_alpha_words
    text.tokenized = timezone.now()
    logger.debug("Saving %s", (n_phrases, n_alpha_words, n_word_tokens))
    text.save(update_fields=['n_word_tokens', 'n_phrases', 'n_alpha_words',
                             'tokenized'])
