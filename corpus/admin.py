import logging

from django import db
from django.contrib import admin, messages
from django.core.paginator import Paginator
from django.db.models import Case, F, FloatField, Q, Value, When, query
from django.db.models.functions import Cast, Reverse

from django.utils import timezone
from django.utils.safestring import mark_safe

from advanced_filters.admin import AdminAdvancedFiltersMixin
from advanced_filters.forms import AdvancedFilterForm
from admin_auto_filters.filters import AutocompleteFilterFactory
from isnull_filter import isnull_filter
from sql_util.utils import SubqueryCount

from corpus.models import DictionaryEntry, TextSentence, WordGrammar


logger = logging.getLogger(__name__)


def fix_clean(cls):
    orig_clean = cls.clean

    def fixed_clean(self):
        for form in self.fields_formset:
            if not form.is_valid():
                form.cleaned_data = {'field': 'id', 'DELETE': True}
        return orig_clean(self)

    cls.clean = fixed_clean


fix_clean(AdvancedFilterForm)
# fix_clean(AdvancedFilterQueryForm)


class DictionaryEntryAdmin(AdminAdvancedFiltersMixin, admin.ModelAdmin):
    ordering = ('lexeme', 'lexeme_no')
    list_display = ('my_lexeme', 'lexeme_no', 'part', 'semgloss',
                    'my_raw_text', 'frequency')
    list_filter = ('part',)
    search_fields = ['lexeme', 'semgloss', 'raw_text']
    advanced_filter_fields = ('lexeme', 'lexeme_no', 'part', 'semgloss',
                              'etym', 'frequency', 'raw_text')

    def my_raw_text(self, obj):
        return mark_safe(obj.html_raw_text())

    @admin.display(description='Лексема', ordering='lexeme')
    def my_lexeme(self, obj):
        return '{} {}' \
            .format(obj.lexeme,
                    DictionaryEntry.HOMONYM_CHOICES[int(obj.lexeme_no)][1])


@admin.action(
    description="Delete selected texts' content keeping metadata intact"
)
def delete_content(modeladmin, request, queryset):
    TextSentence.objects.filter(text__in=queryset).delete()
    queryset.update(n_phrases=0, n_word_tokens=0, n_alpha_words=0,
                    tokenized=None)


@admin.action(description="Tokenize selected texts")
def tokenize(modeladmin, request, queryset):
    ids = list(queryset.values_list('id', flat=True))
    db.connections.close_all()
    from . import tasks
    n = 0
    while ids:
        tasks.retokenize(dict(ids=ids[:1]))
        ids = ids[1:]
        n += 1
    messages.add_message(request, messages.SUCCESS,
                         '%d tokenization tasks enqueued.' % n)


class TextPaginator(Paginator):
    @property
    def count(self):
        if hasattr(self.object_list, 'count'):
            return self.object_list.annotate(
                recognition=Value(0.0, output_field=FloatField()),
            ).count()
        return super().count()


class TextAdmin(admin.ModelAdmin):
    actions = [delete_content, tokenize]
    ordering = ('title',)
    paginator = TextPaginator
    fieldsets = [
        (None, {'fields': [
            'author', 'storyteller',
            'title', 'dialect', 'genre', 'creation_time',
            'record_place', 'collector',
            'publication',
            'city', 'year',
            'editor',
            'translator', 'translation_title',
            'translation_publication',
            'translation_city',
            'translation_year',
            ]}),
        ('Import', {'fields': [
            'datafile',
            'filetype',
            'uploaded',
            'tokenized',
        ]}),
    ]
    list_display = ['title', 'id', 'get_author', 'get_dialect',
                    'translation_title', 'n_phrases',
                    'n_alpha_words', 'n_word_tokens',
                    'get_uploaded', 'get_tokenized', 'get_recognition']
    list_filter = [
        AutocompleteFilterFactory('Dialect', 'dialect'),
        AutocompleteFilterFactory('Genre', 'genre'),
        AutocompleteFilterFactory('Author', 'author'),
        isnull_filter('datafile', 'No file'),
    ]
    readonly_fields = ['uploaded', 'tokenized']
    search_fields = ['title', 'translation_title', 'datafile']

    def save_model(self, request, obj, form, change):
        if 'datafile' in form.changed_data:
            obj.n_phrases = None
            obj.n_word_tokens = None
            obj.n_alpha_words = None
            obj.uploaded = timezone.now() if obj.datafile else None
            obj.tokenized = None
        super(TextAdmin, self).save_model(request, obj, form, change)

    @admin.display(description="Dialect", ordering='dialect__name_ru')
    def get_dialect(self, obj):
        return mark_safe(str(obj.dialect)) if obj.dialect else "-"

    @admin.display(description="Author", ordering='author__last_name')
    def get_author(self, obj):
        return mark_safe(str(obj.author)) if obj.author else "-"

    def get_queryset(self, request):
        orig_qs = super().get_queryset(request)
        qs = orig_qs.select_related('author', 'dialect')
        qs = qs.annotate(
            recognition=SubqueryCount(
                'sentences__words',
                filter=Q(wordform__n_auto_grammars__gte=1),
                output_field=FloatField(),
            ) / Case(When(n_alpha_words__gt=0,
                          then=Cast(F('n_alpha_words'),
                                    output_field=FloatField())),
                     default=1000000,
                     output_field=FloatField())
        )

        class FastCountQuerySet(query.QuerySet):
            def count(self):
                return self.annotate(recognition=None).get_count()
                # Use simpler query for COUNT
                return orig_qs.count()
        return qs._chain(__class__=FastCountQuerySet)

    @admin.display(description="Upload date", ordering='uploaded')
    def get_uploaded(self, obj):
        return obj.uploaded.strftime("%Y.%m.%d %H:%M") if obj.uploaded else '—'

    @admin.display(description="Tokenization date", ordering='tokenized')
    def get_tokenized(self, obj):
        return (obj.tokenized.strftime("%Y.%m.%d %H:%M")
                if obj.tokenized else '-')

    @admin.display(description="Recognition %", ordering='recognition')
    def get_recognition(self, obj):
        value = obj.recognition
        return mark_safe(
            ('<div style="background: #%x%x0; color: #fff; width: %dpx; '
             'border: 1px solid #000; padding: 1px;'
             'text-shadow: 0 -1px 2px #000; text-align: center;">'
             '%d%%</span>') % (
                int(15 - 15 * value), int(15 * value),
                int(80 * value), int(100 * value)
            ) if value is not None else "-"
        )


@admin.action(description="Delete selected text words' confirmed grammar")
def delete_confirmed_grammar(modeladmin, request, queryset):
    queryset.update(confirmed_grammar=None)


class TextWordAdmin(admin.ModelAdmin):
    actions = [delete_confirmed_grammar]
    list_display = ['id', 'wordform', 'confirmed_grammar_id']
    list_filter = [
        AutocompleteFilterFactory('Text', 'sentence__text'),
        isnull_filter('confirmed_grammar', 'Unconfirmed'),
    ]
    readonly_fields = ['sentence', 'wordform', 'confirmed_grammar']
    search_fields = ['wordform__wordform']


def wordgrammar_get_features(instance):
    out = []
    for feat in instance.features.all().order_by('position'):
        out.append('Position %d: <b>%s</b> '
                   '<span style="font-variant: small-caps">%s</span>' %
                   (feat.position,
                    feat.word_feature.affix,
                    feat.word_feature.feature.signature))
    return '\n'.join(out)


class WordGrammarInline(admin.TabularInline):
    model = WordGrammar
    fields = ['id', 'auto', 'lexeme', 'features']
    readonly_fields = ['auto', 'lexeme', 'features']
    extra = 0

    def has_add_permission(self, request, obj):
        return False

    def features(self, instance):
        return mark_safe(wordgrammar_get_features(instance))


MAP = dict(
    eq_0=dict(n_auto_grammars=0),
    gte_1=dict(n_auto_grammars__gte=1),
    eq_1=dict(n_auto_grammars=1),
    gte_2=dict(n_auto_grammars__gte=2),
)


class WordformRecognitionFilter(admin.SimpleListFilter):
    title = 'Разборы словоформы'
    parameter_name = 'recognition'

    def lookups(self, request, model_admin):
        return (
            ('eq_0', '0 разборов'),
            ('gte_1', '1 и более разборов'),
            ('eq_1', '1 разбор'),
            ('gte_2', '2 и более разборов'),
        )

    def queryset(self, request, queryset):
        return (queryset.filter(**(MAP[self.value()])) if self.value()
                else queryset)


class WordformAdmin(admin.ModelAdmin):
    inlines = [WordGrammarInline]
    list_display = ('wordform', 'grammar_infos', 'frequency',
                    'reversed', 'parsed', 'n_auto_grammars', 'corpus_link')
    list_filter = [
        isnull_filter('parsed', 'Never parsed'),
        WordformRecognitionFilter,
        isnull_filter('textword', 'Orphaned'),
        AutocompleteFilterFactory('Text', 'textword__sentence__text'),
        AutocompleteFilterFactory('Feature',
                                  'grammars__features__word_feature__feature')
    ]
    search_fields = [
        'wordform',
        'grammars__key',
        'grammars__lexeme__semgloss',
    ]

    def get_queryset(self, request):
        return super().get_queryset(request) \
            .annotate(
                reversed=Reverse('wordform'),
            ) \
            .prefetch_related('grammars')

    def grammar_infos(self, instance):
        s = '<br/><br/>'.join(
            '%s <br/>%s' % (str(grammar.lexeme),
                            wordgrammar_get_features(grammar).replace('\n',
                                                                      '<br/>'))
            for grammar in instance.grammars.all()
        )
        return mark_safe('<div style="font-family: serif">%s</div>' % s)

    @admin.display(ordering='reversed')
    def reversed(self, instance):
        return instance.reversed

    def corpus_link(self, instance):
        return mark_safe(
            '<a href="/corpus/?word1_wordform=%s&word1_wordform_lookup=iexact"'
            ' target="_blank">в корпусе</a>' %
            instance.wordform
        )


class AffixFeatureAdmin(admin.ModelAdmin):
    list_display = ('signature', 'name_en', 'name_ru')
    list_filter = [
        isnull_filter('wordfeature', 'Orphaned'),
    ]
    search_fields = ('signature', 'name_en', 'name_ru')


class WordFeatureAdmin(admin.ModelAdmin):
    list_display = ('affix', 'feature_')
    list_filter = [
        isnull_filter('wordgrammarfeature', 'Orphaned'),
    ]
    readonly_fields = ('affix', 'feature')
    search_fields = ('affix', 'feature__signature')

    def feature_(self, instance):
        return mark_safe(str(instance.feature))


class PersonAdmin(admin.ModelAdmin):
    list_display = ('name', 'birth_year', 'birth_place', 'notes', 'slug')
    ordering = ('last_name', 'first_name')
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ['last_name']


class OrderedAdmin(admin.ModelAdmin):
    ordering = ('name_ru',)
    search_fields = ['name_ru']


class PublicationAdmin(admin.ModelAdmin):
    filter_horizontal = ('authors', 'compilers', 'editors')
