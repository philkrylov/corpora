import docx

from .tokenizer import split_words


def docx_word_iterator(words):
    return ((w, None) for w in words)


def docx_sentence_iterator(table, refs=False, phonetic=False):
    def get_column_data(column):
        return docx.table._Cell(column, table).text.strip()

    for tr in table._tbl.tr_lst:
        ref, source_phonetic = None, None
        if refs and phonetic:
            ref_tc, source_phonetic_tc, source_tc, target_tc = tr.tc_lst
            ref = get_column_data(ref_tc)
            source_phonetic = get_column_data(source_phonetic_tc)
        elif refs:
            ref_tc, source_tc, target_tc = tr.tc_lst
            ref = get_column_data(ref_tc)
        elif phonetic:
            source_phonetic_tc, source_tc, target_tc = tr.tc_lst
            source_phonetic = get_column_data(source_phonetic_tc)
        else:
            try:
                _, source_tc, target_tc = tr.tc_lst
            except ValueError:
                source_tc, target_tc = tr.tc_lst
        yield (dict(translation_ru=get_column_data(target_tc),
                    source_phonetic=source_phonetic,
                    reference=ref),
               docx_word_iterator(split_words(get_column_data(source_tc))))


def docx_paragraph_iterator(docx_file, refs=False, phonetic=False):
    doc = docx.Document(docx_file)
    for table in doc.tables:
        yield docx_sentence_iterator(table, refs=refs, phonetic=phonetic)
