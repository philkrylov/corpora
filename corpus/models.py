# -*- coding: utf-8 -*-

import re

from django.db import models
from django.utils.deconstruct import deconstructible
from django.utils.safestring import mark_safe


star_to_html_re = re.compile(r'\\[BCHILUbchilu]|\&|<')
star_to_html_dict = {r'\B': '<b>',
                     r'\C': '<font size="-1">',
                     r'\H': '<sup>',
                     r'\I': '<i>',
                     r'\L': '<sub>',
                     '\\U': '<u>',
                     r'\b': '</b>',
                     r'\c': '</font>',
                     r'\h': '</sup>',
                     r'\i': '</i>',
                     r'\l': '</sub>',
                     '\\u': '</u>',
                     '&': '&amp;',
                     '<': '&lt;'}


def star_to_html(s):
    return star_to_html_re.sub(lambda m: star_to_html_dict[m.group(0)], s)


class DictionaryEntry(models.Model):
    HOMONYM_CHOICES = tuple((x, x) for x in ('', 'I', 'II', 'III', 'IV', 'V',
                                             'VI', 'VII', 'VIII', 'IX', 'X'))
    PART_CHOICES = (('', ''),
                    ('NOMEN', 'имя'),
                    ('VERBUM', 'глагол'),
                    ('союз', 'союз'),
                    ('межд.', 'междометие'),
                    ('нареч.', 'наречие'),
                    ('част.', 'частица'))
    raw_text = models.TextField('Исходная словарная статья', null=True,
                                blank=True)
    lexeme = models.CharField('Лексема', max_length=256, db_index=True)
    lexeme_no = models.CharField('Номер омонима', null=True, blank=True,
                                 max_length=5, choices=HOMONYM_CHOICES)
    raw_stem = models.CharField('Основа глагола (по словарю)', null=True,
                                blank=True, max_length=256)
    stem = models.CharField('Основа имени', null=True, blank=True,
                            max_length=256)
    form = models.CharField('Словоформа со стяжением', null=True, blank=True,
                            max_length=256)
    deriv = models.CharField('Словообразовательные элементы',
                             null=True, blank=True, max_length=256)
    derivgloss = models.CharField('Словообразовательная глоссировка',
                                  null=True, blank=True, max_length=256)
    semgloss = models.CharField('Перевод',
                                null=True, blank=True, max_length=256)
    part = models.CharField('Часть речи', null=True, blank=True, max_length=16,
                            choices=PART_CHOICES)
    phraseology = models.TextField('Фразеология', null=True, blank=True)
    frequency = models.PositiveIntegerField('Частотность',
                                            default=0, db_index=True)

    class Meta:
        ordering = ['lexeme', 'lexeme_no']
        verbose_name_plural = 'dictionary entries'

    def __str__(self):
        return '{}{} \u201B{}\u2019'.format(self.lexeme,
                                            ((' %s' % self.lexeme_no)
                                             if self.lexeme_no else ''),
                                            self.semgloss)

    def html_raw_text(self):
        return star_to_html(self.raw_text[:60] + '...')


class Wordform(models.Model):
    wordform = models.CharField("Wordform",
                                max_length=256, blank=False, unique=True)
    frequency = models.PositiveIntegerField("Frequency", default=0,
                                            db_index=True)
    parsed = models.DateTimeField("Parse date", null=True, blank=True,
                                  db_index=True, editable=False)
    n_auto_grammars = models.PositiveIntegerField("Analyses count",
                                                  db_index=True, default=0)

    def __str__(self):
        return self.wordform


class AffixFeature(models.Model):
    signature = models.CharField("Signature",
                                 max_length=32, blank=False, unique=True)
    name_en = models.CharField("Name (English)",
                               max_length=64, null=True, blank=True)
    name_ru = models.CharField("Name (Russian)",
                               max_length=64, null=True, blank=True)

    class Meta:
        ordering = ['signature']

    def __str__(self):
        affixes = [wfeat.affix for wfeat in self.wordfeature_set.all()]
        return mark_safe(' '.join([
            '<span class="sc">%s</span>' % self.signature,
            '<span class="mtxt">%s</span>' % ' | '.join(affixes)
        ]))


class WordFeature(models.Model):
    affix = models.CharField("Affix", max_length=32, null=False, blank=False)
    feature = models.ForeignKey(AffixFeature, on_delete=models.PROTECT,
                                verbose_name="Feature")

    class Meta:
        unique_together = [('affix', 'feature')]

    def __str__(self):
        return self.affix


class WordGrammar(models.Model):
    wordform = models.ForeignKey(Wordform, on_delete=models.CASCADE,
                                 verbose_name="Wordform",
                                 related_name='grammars')
    auto = models.BooleanField("Automatic parse result")
    lexeme = models.ForeignKey(DictionaryEntry, on_delete=models.SET_NULL,
                               verbose_name="Lexeme", null=True, blank=True)
    key = models.CharField("Index key", max_length=512, db_index=True)
    raw_stem = models.CharField('Stem (if missing in dictionary)',
                                max_length=256, null=True, blank=True)
    raw_semgloss = models.CharField('Semantics (if missing in dictionary)',
                                    max_length=256, null=True, blank=True)
    weight = models.SmallIntegerField('Weight (feature count)')

    def __str__(self):
        return str(self.lexeme)

    class Meta:
        index_together = ('wordform', 'weight')
        ordering = ('wordform', 'weight')


class WordGrammarFeature(models.Model):
    word_grammar = models.ForeignKey(WordGrammar, on_delete=models.CASCADE,
                                     verbose_name="Wordform grammar",
                                     related_name="features")
    position = models.IntegerField("Position within wordform",
                                   db_index=True)
    word_feature = models.ForeignKey(WordFeature, on_delete=models.PROTECT,
                                     verbose_name="Affix and its feature")

    def __str__(self):
        return '%s' % self.position

    class Meta:
        ordering = ('position',)


class Place(models.Model):
    name_ru = models.CharField("Name (Russian)",
                               max_length=128, null=False, blank=False)
    name_en = models.CharField("Name (English)",
                               max_length=128, null=True, blank=True)

    def __str__(self):
        return self.name_ru


class Person(models.Model):
    name = models.CharField("Name", max_length=100, blank=False)
    first_name = models.CharField("First name",
                                  max_length=100, null=True, blank=True)
    last_name = models.CharField("Last name",
                                 max_length=100, null=True, blank=True)
    birth_year = models.IntegerField("Birth year", null=True, blank=True)
    birth_place = models.ForeignKey(Place, verbose_name="Birth place",
                                    null=True, blank=True,
                                    on_delete=models.PROTECT)
    death_year = models.IntegerField("Death year", null=True, blank=True)
    notes = models.TextField("Notes", null=True, blank=True)
    slug = models.SlugField("Slug", null=True, blank=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return mark_safe('<i>{}</i>'.format(re.sub(r'(\([а-я][^)]+\))',
                                                   r'</i>\1<i>', self.name)))


class Genre(models.Model):
    name_ru = models.CharField("Name (Russian)",
                               max_length=128, null=False, blank=False)
    name_en = models.CharField("Name (English)",
                               max_length=128, null=True, blank=True)

    def __str__(self):
        return self.name_ru


class Dialect(models.Model):
    name_ru = models.CharField("Name (Russian)",
                               max_length=128, null=False, blank=False)
    name_en = models.CharField("Name (English)",
                               max_length=128, null=True, blank=True)

    def __str__(self):
        return self.name_ru


class Publication(models.Model):
    authors = models.ManyToManyField(Person, verbose_name="Authors",
                                     related_name="publications_as_author",
                                     blank=True)
    title = models.CharField("Title", max_length=500, null=False, blank=False)
    city = models.CharField("Publication city",
                            max_length=64, null=True, blank=True)
    publishing_house = models.CharField("Publishing house",
                                        max_length=500, null=True, blank=True)
    year = models.IntegerField("Publication year", null=True, blank=True)
    compilers = models.ManyToManyField(Person, verbose_name="Compilers",
                                       related_name="publications_as_compiler",
                                       blank=True)
    editors = models.ManyToManyField(Person, verbose_name="Editors",
                                     related_name="publications_as_editor",
                                     blank=True)
    series = models.CharField("Series", max_length=500, null=True, blank=True)
    volume = models.CharField("Volume", max_length=64, null=True, blank=True)

    def __str__(self):
        parts = (
            ', '.join(str(a) for a in self.authors.all()),
            ((', '.join(str(c) for c in self.compilers.all()) + ' (запись)')
             if self.compilers.count() else ''),
            ((', '.join(str(e) for e in self.editors.all()) + ' (ред.)')
             if self.editors.count() else ''),
            self.title,
            ', '.join((': '.join(v for v in (self.city, self.publishing_house)
                                 if v),
                      str(self.year))),
            ('({})'.format('. — '.join((self.series, self.volume))
                           if self.volume else self.series)
             if self.series else ''),
        )
        return mark_safe('. '.join(s for s in parts if s))

    class Meta:
        ordering = ['year', 'title']


@deconstructible
class UploadTo(object):
    def __init__(self, path):
        self.path = path

    def __call__(self, instance, filename):
        if instance.pk:
            prefix = str(instance.pk) + '_'
            if not filename.startswith(prefix):
                filename = prefix + filename
        return self.path + filename


class Text(models.Model):
    publication = models.ForeignKey(Publication, on_delete=models.PROTECT,
                                    verbose_name="Publication",
                                    related_name="texts",
                                    null=True, blank=True)
    translation_publication = \
        models.ForeignKey(Publication, on_delete=models.PROTECT,
                          verbose_name="Translation publication",
                          related_name="texts_as_translation",
                          null=True, blank=True)
    author = models.ForeignKey(Person, on_delete=models.PROTECT,
                               verbose_name="Author", related_name="texts",
                               null=True, blank=True)
    title = models.CharField("Title", max_length=500, null=False, blank=False)
    dialect = models.ForeignKey(Dialect, on_delete=models.PROTECT,
                                verbose_name="Dialect", related_name="texts",
                                null=True, blank=True)
    storyteller = models.ForeignKey(Person, on_delete=models.PROTECT,
                                    verbose_name="Storyteller",
                                    related_name="texts_as_storyteller",
                                    null=True, blank=True)
    record_place = models.ForeignKey(Place, on_delete=models.PROTECT,
                                     verbose_name="Recording place",
                                     related_name="texts",
                                     null=True, blank=True)
    genre = models.ForeignKey(Genre, on_delete=models.PROTECT,
                              verbose_name="Genre", related_name="texts")
    creation_time = models.CharField("Time of writing",
                                     max_length=128, null=True, blank=True)
    city = models.CharField("Publication city",
                            help_text="Устарело, перенести в Publication",
                            max_length=64, null=True, blank=True)
    year = models.IntegerField("Publication year",
                               help_text="Устарело, перенести в Publication",
                               null=True, blank=True)
    editor = models.ForeignKey(Person, on_delete=models.PROTECT,
                               verbose_name="Editor",
                               related_name="texts_as_editor",
                               null=True, blank=True)
    n_word_tokens = models.PositiveIntegerField("Word tokens",
                                                null=True, blank=True)
    n_alpha_words = models.PositiveIntegerField("Letter word tokens",
                                                null=True, blank=True)
    n_phrases = models.PositiveIntegerField("Phrases", null=True, blank=True)
    translator = models.CharField("Translator",
                                  max_length=64, null=True, blank=True)
    translation_title = models.CharField("Translation title",
                                         max_length=500,
                                         null=True, blank=True)
    translation_city = models.CharField(
        "Translation publication city",
        help_text="Устарело, перенести в Translation publication",
        max_length=64, null=True, blank=True,
    )
    translation_year = models.PositiveIntegerField(
        "Translation publication year",
        help_text="Устарело, перенести в Translation publication",
        null=True,
        blank=True,
    )
    collector = models.ForeignKey(Person, on_delete=models.PROTECT,
                                  verbose_name="Collector",
                                  related_name="texts_as_collector",
                                  null=True, blank=True)
    datafile = models.FileField("Data file", null=True, blank=True,
                                max_length=512,
                                upload_to=UploadTo("textdata/"))
    filetype = models.CharField(
        "Data file type",
        choices=(
            ("docx", ".docx table with 2 columns: (source, translation)"),
            ("docx_3",
             ".docx table with 3 columns: (reference, source, translation)"),
            ("docx_p",
             ".docx table with 3 columns: (phonetic, source, translation)"),
            ("docx_4", (".docx table with 4 columns:"
                        " (reference, phonetic, source, translation)")),
        ),
        null=True,
        max_length=6,
        blank=True,
    )
    uploaded = models.DateTimeField("Upload date", null=True)
    tokenized = models.DateTimeField("Tokenization date", null=True)

    def __str__(self):
        parts = (str(self.author) if self.author else None,
                 '%s %s (%s)' % (self.title,
                                 (('// %s' % self.translation_title)
                                  if self.translation_title else ''),
                                 self.genre or ''),
                 ('%d словоупотреблений, %d фраз' %
                  (self.n_alpha_words or 0, self.n_phrases or 0)))
        return mark_safe('. '.join(s for s in parts if s))

    class Meta:
        ordering = ['author__name', 'year', 'title']


class TextSentence(models.Model):
    paragraph_number = \
        models.PositiveIntegerField("Paragraph number within text")
    text = models.ForeignKey(Text, on_delete=models.CASCADE,
                             verbose_name="Text", related_name="sentences")
    number = models.PositiveIntegerField("Sentence number within paragraph")
    source_phonetic = models.TextField(
        "Source sentence (phonetic representation)",
        null=True, blank=True,
    )
    translation_en = models.TextField("English translation",
                                      null=True, blank=True)
    translation_ru = models.TextField("Russian translation",
                                      null=True, blank=True)
    reference = models.TextField("Reference address", null=True)

    def __str__(self):
        return "<TextSentence {}:{}.{}:{}>".format(self.id,
                                                   self.paragraph.number,
                                                   self.number,
                                                   self.translation_ru)

    def previous(self):
        if self.number == 1:
            if self.paragraph_number == 1:
                return None
            else:
                return TextSentence.objects \
                    .filter(text_id=self.text_id,
                            paragraph_number=self.paragraph_number - 1) \
                    .order_by('-number').first()
        else:
            return TextSentence.objects.get(
                text_id=self.text_id,
                paragraph_number=self.paragraph_number,
                number=self.number - 1,
            )

    def next(self):
        try:
            return TextSentence.objects.get(
                text_id=self.text_id,
                paragraph_number=self.paragraph_number,
                number=self.number + 1,
            )
        except TextSentence.DoesNotExist:
            try:
                return TextSentence.objects.get(
                    text=self.text,
                    paragraph_number=self.paragraph_number + 1,
                    number=1,
                )
            except TextSentence.DoesNotExist:
                return None


class TextWord(models.Model):
    sentence = models.ForeignKey(TextSentence, on_delete=models.CASCADE,
                                 verbose_name="Sentence", related_name="words")
    number = models.PositiveIntegerField("Word number within sentence")
    clause = models.PositiveIntegerField("Clause")
    leftpunct = models.CharField("Left punctuation",
                                 max_length=8, null=True, blank=True)
    wordform = models.ForeignKey(Wordform, on_delete=models.PROTECT,
                                 verbose_name="Wordform")
    rightpunct = models.CharField("Right punctuation",
                                  max_length=8, null=True, blank=True)
    confirmed_grammar = \
        models.ForeignKey(WordGrammar,
                          on_delete=models.SET_NULL,
                          verbose_name="Confirmed grammar info",
                          null=True, blank=True)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return '%d:%s' % (self.pk, '')  # self.wordform)
