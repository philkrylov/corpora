# -*- coding: utf-8 -*-

from dataclasses import dataclass
from functools import lru_cache as memoize
from itertools import groupby
from operator import attrgetter
import logging
import re

from django import forms
from django.core.cache import caches
from django.db.models import BooleanField, Prefetch, Min, Case, When, Value, Q
from django.forms.widgets import CheckboxSelectMultiple, TextInput, NumberInput
from django.views import generic

import django_filters as filters
from django_filters.filterset import FilterSetMetaclass

from rest_framework import generics, pagination
from .serializers import TextSerializer, TextSentenceSerializer


from .models import (
        AffixFeature,
        DictionaryEntry,
        Person,
        Text,
        TextSentence,
        TextWord,
        WordGrammar,
        WordGrammarFeature,
)


logger = logging.getLogger(__name__)


LOOKUP_CHOICES = [
    ('iexact', 'Точное совпадение'),
    ('icontains', 'Подстрока'),
    ('istartswith', 'Начало строки'),
    ('iendswith', 'Конец строки'),
    ('iregex', 'Регулярное выражение'),
]


def CharmapTextInput(class_):
    return TextInput(attrs={'class': 'charmap %s' % class_})


ContextWidthInput = NumberInput(attrs={'min': 0, 'max': 5, 'placeholder': '0'})
PageNumberInput = NumberInput(attrs={'min': 1, 'placeholder': '1'})
DistanceInput = NumberInput(attrs={'min': 0, 'max': 5, 'placeholder': '—'})


class TextSentencePagination(pagination.PageNumberPagination):
    page_size = 500
    max_page_size = 1000


class TextSelectMultiple(CheckboxSelectMultiple):
    template_name = 'khakas/widgets/checkbox_select_text.html'
    option_template_name = 'khakas/widgets/checkbox_option_text.html'


class TextMultipleChoiceField(filters.fields.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj


class TextMultipleChoiceFilter(filters.ModelMultipleChoiceFilter):
    field_class = TextMultipleChoiceField


class TextSentenceFilterSet(filters.FilterSet):
    translation_ru = filters.CharFilter(
        label='Русский перевод предложения (подстрока)',
        lookup_expr='icontains',
        widget=CharmapTextInput('translation_ru'),
    )
    text = TextMultipleChoiceFilter(
        field_name='text',
        label='Подкорпус',
        queryset=(Text.objects.filter(n_phrases__gt=0)
                  .select_related(
                      'genre',
                      'dialect',
                      'author',
                      'storyteller',
                      'storyteller__birth_place',
                      'collector',
                      'editor',
                      'record_place',
                  )
                  .order_by('author__last_name', 'title')),
        widget=TextSelectMultiple,
    )
    context_width = filters.NumberFilter(
        label='Ширина контекста (число фраз до и после)',
        method='filter_context_width',
        initial=0,
        widget=ContextWidthInput,
    )
    page = filters.NumberFilter(
        label='Страница',
        method='filter_page',
        initial=1,
        widget=forms.HiddenInput(),
    )

    def filter_context_width(self, qs, name, value):
        self.context_width = value
        return qs

    def filter_page(self, qs, name, value):
        self.page = value
        return qs

    class Meta:
        model = TextSentence
        fields = []


@memoize()
def text_word_filter(index):
    prefix = 'word%d_' % index
    feature_qs = AffixFeature.objects \
        .filter(wordfeature__wordgrammarfeature__word_grammar__auto=True) \
        .prefetch_related('wordfeature_set') \
        .annotate(n_slot=Min('wordfeature__wordgrammarfeature__position')) \
        .order_by('n_slot', 'signature')

    def noop_method(qs, name, value):
        return qs

    @property
    def qs(self):
        get = self.request.GET
        qs = super(self.__class__, self).qs
        conds = []

        lexeme, lookup = (get.get(prefix + 'lexeme_auto'),
                          get.get(prefix + 'lexeme_auto_lookup', 'iexact'))
        if lexeme:
            conds.append(Q(**{'lexeme__lexeme__' + lookup: lexeme}))

        features_auto = [int(s) for s in get.getlist(prefix + 'features_auto')]
        if features_auto:
            features_auto = groupby(feature_qs.filter(id__in=features_auto),
                                    attrgetter('n_slot'))
            by_slots = {n_slot: list(g) for n_slot, g in features_auto}
            for affixes in by_slots.values():
                conds.append(Q(features__word_feature__feature__in=affixes))

        empty_slots_auto = get.getlist(prefix + 'empty_slots_auto')
        if empty_slots_auto:
            conds.append(~Q(features__position__in=empty_slots_auto))

        semgloss, lookup = (get.get(prefix + 'semgloss_auto'),
                            get.get(prefix + 'semgloss_auto_lookup', 'iexact'))
        if semgloss:
            conds.append(Q(**{'lexeme__semgloss__' + lookup: semgloss}))

        if conds:
            grammar_qs = WordGrammar.objects
            for cond in conds:
                grammar_qs = grammar_qs.filter(cond)
            return qs.filter(wordform__grammars__in=grammar_qs)
        return qs

    return FilterSetMetaclass('TextWordFilter', (filters.FilterSet,), {
        'Meta': type('Meta', (), dict(model=TextWord, fields=[])),
        prefix + 'distance': filters.NumberFilter(
            label='Максимальное расстояние',
            widget=DistanceInput,
            method=noop_method,
        ),
        prefix + 'wordform': filters.LookupChoiceFilter(
            field_name='wordform__wordform',
            field_class=forms.CharField,
            lookup_choices=LOOKUP_CHOICES,
            empty_label=None,
            label='Словоформа (в хакасской орфографии)',
            widget=CharmapTextInput('wordform'),
        ),
        prefix + 'lexeme_auto': filters.LookupChoiceFilter(
            field_name='wordform__grammars__lexeme__lexeme',
            field_class=forms.CharField,
            lookup_choices=LOOKUP_CHOICES,
            empty_label=None,
            label='Лемма (в хакасской орфографии)',
            widget=CharmapTextInput('lexeme_auto'),
            method=noop_method,
        ),
        prefix + 'features_auto': filters.ModelMultipleChoiceFilter(
            field_name='wordform__grammars__features__word_feature__feature',
            label='Аффиксы (автоматическая разметка)',
            queryset=feature_qs,
            conjoined=True,
            widget=CheckboxSelectMultiple(attrs={'class': 'features_auto'}),
            method=noop_method,
        ),
        prefix + 'empty_slots_auto': filters.MultipleChoiceFilter(
            choices=[(n, '—') for n, in (WordGrammarFeature.objects
                                         .distinct('position')
                                         .values_list('position'))],
            label='Пустые слоты',
            widget=CheckboxSelectMultiple(attrs={'class': 'empty_slots_auto'}),
            method=noop_method,
        ),
        prefix + 'semgloss_auto': filters.LookupChoiceFilter(
            field_name='wordform__grammars__lexeme__semgloss',
            field_class=forms.CharField,
            lookup_choices=LOOKUP_CHOICES,
            empty_label=None,
            label='Русский перевод леммы',
            widget=CharmapTextInput('semgloss_auto'),
            method=noop_method,
        ),
        'qs': qs,
        # features_or = filters.ModelMultipleChoiceFilter(
        #     field_name='words__confirmed_grammar__features__word_feature__feature',
        #     label='Features (any)',
        #     queryset=AffixFeature.objects.all(),
        # )
        # features_and = filters.ModelMultipleChoiceFilter(
        #     field_name='confirmed_grammar__features__word_feature__feature',
        #     label='Аффиксы (ручная разметка)',
        #     queryset=AffixFeature.objects.filter(
        #         wordfeature__wordgrammarfeature__word_grammar__auto=False,
        #     ).distinct(),
        #     conjoined=True,
        #     widget=CheckboxSelectMultiple,
        # )
    })


def refilter(qs, matches):
    return qs.filter(sentence_id__in=set([m.sid for m in matches]))


def make_word_filter(idx, data, qs, request, common_matches=None):
    if 'word%d_features_auto' % idx in request.GET:
        qs = qs.filter(confirmed_grammar__isnull=True)
    elif 'word%d_features_and' % idx in request.GET:
        qs = qs.filter(confirmed_grammar__isnull=False)
    if idx > 1 and common_matches is not None:
        qs = refilter(qs, common_matches)
    return text_word_filter(idx)(data, queryset=qs, request=request)


def get_n_word_filters(data):
    return max(1, len(set(k.split('_')[0] for k in data.keys()
                          if k.startswith('word'))))


@dataclass(frozen=True)
class TextWordMatch:
    sid: int
    twid: int
    number: int
    idx: int
    __slots__ = ('sid', 'twid', 'number', 'idx')


def get_word_filter_matches(f, idx,
                            common_matches=None,
                            distance=None):
    def distance_ok(this_match, prev_match):
        return (1 <= this_match.number - prev_match.number <=
                distance + 1)

    f_qs = f.qs
    match_list = \
        set(TextWordMatch(*row, idx)
            for row in f_qs.values_list('sentence_id', 'id', 'number'))
    if idx == 1:
        return match_list

    this_matches = set()
    for sid, g in groupby(match_list, key=attrgetter('sid')):
        for this_match in set(g):
            if distance is None:
                this_matches.add(this_match)
            else:
                for prev_match in (m for m in common_matches if
                                   m.sid == sid and m.idx == idx - 1):
                    if distance_ok(this_match, prev_match):
                        this_matches.add(this_match)
    prev_matches = (m for m in common_matches
                    if m.sid in (tm.sid for tm in this_matches))
    if distance is not None:
        prev_matches = (m for m in prev_matches
                        if any(m.sid == tm.sid and distance_ok(tm, m)
                               for tm in this_matches))
    return this_matches | set(prev_matches)


def get_results(data, request):
    local = caches['local']
    key = data.urlencode()
    cached = local.get(key)
    if cached:
        return cached

    wgf_qs = WordGrammarFeature.objects \
        .select_related('word_feature', 'word_feature__feature') \
        .order_by('position')
    grammar_qs = WordGrammar.objects \
        .select_related('lexeme') \
        .prefetch_related(Prefetch('features', queryset=wgf_qs))
    word_qs = TextWord.objects \
        .select_related('confirmed_grammar', 'confirmed_grammar__lexeme',
                        'wordform') \
        .prefetch_related(Prefetch('wordform__grammars',
                                   queryset=grammar_qs),
                          Prefetch('confirmed_grammar__features',
                                   queryset=wgf_qs))

    '''
    if 'features_auto_and' in self.request.GET:
        word_qs = word_qs.filter(wordform__grammars__auto=True)
    '''

    ru = data.get('translation_ru')
    sentence_filter = TextSentenceFilterSet(data)
    text_qs = TextSentenceFilterSet.declared_filters['text'].queryset \
        .select_related('author')
    if ru or len(data.getlist('text')) < text_qs.count():
        first_word_qs = word_qs.filter(sentence__in=sentence_filter.qs)
    else:
        first_word_qs = word_qs

    n_word_filters = get_n_word_filters(data)
    word_filters = []

    word_filters.append(make_word_filter(1, data, first_word_qs, request))
    common_matches = get_word_filter_matches(word_filters[0], 1)

    for idx, f in (
        (i, make_word_filter(i, data, word_qs,
                             request, common_matches))
        for i in range(2, n_word_filters + 1)
    ):
        word_filters.append(f)
        logger.debug('filter %d: %s' % (idx, f.qs.query))
        distance = data.get('word%d_distance' % idx) or None
        if distance:
            distance = int(distance)
        common_matches = \
            get_word_filter_matches(f, idx,
                                    common_matches, distance)
        if not common_matches:
            break

    text_word_ids = set([m.twid for m in common_matches])
    sentence_ids = set([m.sid for m in common_matches])
    n_sentence_matches = len(sentence_ids)

    context_width = int(data.get('context_width') or '0')
    if context_width:
        expanded_ids = []
        for sentence in TextSentence.objects \
                .filter(id__in=sentence_ids) \
                .only('paragraph_number', 'text_id', 'number'):
            expanded_ids.append(sentence.id)
            for step in (TextSentence.previous, TextSentence.next):
                neighbour = sentence
                for n in range(context_width):
                    neighbour = step(neighbour)
                    if not neighbour:
                        break
                    expanded_ids.append(neighbour.id)
        sentence_ids = set(expanded_ids)

    cached = (sentence_ids, n_sentence_matches, text_word_ids)
    local.set(key, cached)
    return cached


class SentenceView(generics.ListAPIView):
    pagination_class = TextSentencePagination
    serializer_class = TextSentenceSerializer
    queryset = TextSentence.objects \
        .prefetch_related(
            Prefetch('words',
                     queryset=TextWord.objects.select_related('wordform')),
            Prefetch('words__wordform__grammars',
                     queryset=WordGrammar.objects.select_related('lexeme')
                     .prefetch_related(
                         Prefetch('features',
                                  queryset=WordGrammarFeature.objects
                                  .select_related('word_feature',
                                                  'word_feature__feature')))),
        )
    '''
            .annotate(
                textword_id=F('words__id'),
                leftpunct=F('words__leftpunct'),
                rightpunct=F('words__rightpunct'),
                confirmed_grammar_id=F('words__confirmed_grammar_id'),
                wordform=F('words__wordform__wordform'),
                grammar_id=F('words__wordform__grammars__id'),
                auto=Coalesce('words__confirmed_grammar__auto',
                              'words__wordform__grammars__auto'),
                raw_semgloss=Coalesce(
                    'words__confirmed_grammar__raw_semgloss',
                    'words__wordform__grammars__raw_semgloss',
                ),
                raw_stem=Coalesce('words__confirmed_grammar__raw_stem',
                                  'words__wordform__grammars__raw_stem'),
                lexeme=Coalesce('words__confirmed_grammar__lexeme__lexeme',
                                'words__wordform__grammars__lexeme__lexeme'),
                lexeme_stem=Coalesce(
                    'words__confirmed_grammar__lexeme__raw_stem',
                    'words__wordform__grammars__lexeme__raw_stem'
                ),
                semgloss=Coalesce(
                    'words__confirmed_grammar__lexeme__semgloss',
                    'words__wordform__grammars__lexeme__semgloss'
                ),
                feature_id=Coalesce('words__confirmed_grammar__features__id',
                                    'words__wordform__grammars__features__id'),
                affix_pos=Coalesce(
                    'words__confirmed_grammar__features__position',
                    'words__wordform__grammars__features__position'
                ),
                affix=Coalesce(
                    'words__confirmed_grammar__features__word_feature__affix',
                    'words__wordform__grammars__features__word_feature__affix'
                ),
                signature=Coalesce(
                    ('words__confirmed_grammar__features__word_feature'
                     '__feature__signature'),
                    ('words__wordform__grammars__features__word_feature'
                     '__feature__signature'),
                ),
            )
    '''
    ordering = ('id',)

    def get_queryset(self):
        text_qs = TextSentenceFilterSet.declared_filters['text'].queryset

        data = self.request.GET.copy()
        if 'fbclid' in data:
            del data['fbclid']
        if 'page' in data:
            del data['page']

        no_query = len(data) == 0
        if no_query:
            data.setlist('text', text_qs.values_list('pk', flat=True))

        sentence_ids, self.n_sentence_matches, text_word_ids = \
            ({}, 0, {}) if no_query else get_results(data, self.request)

        self.n_word_matches = len(text_word_ids)
        self.sentence_translation_match = data.get('translation_ru')

        every_text_word = (len(text_word_ids) >= TextWord.objects.count())
        sentences = TextSentence.objects \
            .filter(id__in=sentence_ids) \
            .select_related('text', 'text__author') \
            .prefetch_related(
                Prefetch('words',
                         queryset=TextWord.objects
                         .select_related('wordform')
                         .annotate(
                             is_match=(Case(When(id__in=text_word_ids,
                                                 then=Value(True)),
                                            default=Value(None),
                                            output_field=BooleanField())
                                       if not every_text_word
                                       else Value(True)),
                         )),
                Prefetch('words__wordform__grammars',
                         queryset=WordGrammar.objects.select_related('lexeme')
                         .prefetch_related(
                             Prefetch('features',
                                      queryset=WordGrammarFeature.objects
                                      .select_related(
                                          'word_feature',
                                          'word_feature__feature',
                                      )))),
            )
        return sentences.order_by('text__author__last_name', 'text__title',
                                  'id')

    def list(self, request, *args, **kwargs):
        response = super(SentenceView, self).list(request, args, kwargs)
        response.data.update({'n_sentence_matches': self.n_sentence_matches,
                              'n_word_matches': self.n_word_matches})
        return response


class TextView(generics.ListAPIView):
    serializer_class = TextSerializer
    queryset = TextSentenceFilterSet.declared_filters['text'].queryset
    ordering = ('author__last_name', 'title')


class CorpusView(generic.TemplateView):
    template_name = 'corpus.html'

    def get_context_data(self, **kwargs):
        error = ''

        # logger.info('%s', unicode(sentences.query))
        text_qs = TextSentenceFilterSet.declared_filters['text'].queryset
        logger.debug('text_qs done')

        request_data = self.request.GET
        if not len(request_data):
            request_data = request_data.copy()
            request_data.setlist('text', text_qs.values_list('pk', flat=True))

        n_word_filters = get_n_word_filters(request_data)
        word_filters = [make_word_filter(i, request_data,
                                         TextWord.objects.all(), self.request)
                        for i in range(1, n_word_filters + 1)]
        logger.debug('word_filters done')
        sentence_filter = TextSentenceFilterSet(request_data)

        sentence_view = SentenceView.as_view()(self.request)
        data = sentence_view.data

        logger.debug('data done')
        page = int(self.request.GET.get('page', 1))
        naked_url = '/corpus/?{}&page=' \
            .format(re.sub(r'&?(fbclid|page)=[^&]+', '',
                           self.request.GET.urlencode()))
        data['page'] = page
        n_last_page = (data['n_sentence_matches'] + 500 - 1) // 500
        data['n_last_page'] = n_last_page
        data['first_page'] = (naked_url + '1') if page > 1 else ''
        data['prev_page'] = (naked_url + str(page - 1)) if page > 1 else ''
        data['next_page'] = \
            (naked_url + str(page + 1)) if page < n_last_page else ''
        data['last_page'] = \
            (naked_url + str(n_last_page)) if (page < n_last_page) else ''

        affixes = AffixFeature.objects.filter(
            wordfeature__wordgrammarfeature__word_grammar__auto=True,
        ).distinct() \
            .prefetch_related('wordfeature_set') \
            .annotate(pos=Min('wordfeature__wordgrammarfeature__position')) \
            .order_by('pos', 'signature')

        logger.debug('returning')
        return dict(word_filters=word_filters,
                    error=error,
                    sentence_filter=sentence_filter,
                    flatpage=None,
                    not_searched_yet=not self.request.GET,
                    data=data,
                    affixes=list(affixes),
                    texts=TextView.as_view()(self.request).data)


class TextsView(generic.ListView):
    template_name = 'texts.html'
    model = Text
    queryset = Text.objects \
        .filter(n_word_tokens__gt=0) \
        .select_related('author', 'genre', 'collector', 'editor',
                        'storyteller') \
        .order_by('author__name', 'title')


class DictionaryEntryDetailView(generic.DetailView):
    template_name = 'dict_detail.html'
    model = DictionaryEntry


class PersonView(generic.ListView):
    template_name = 'persons.html'
    model = Person
    queryset = (
        Person.objects
        .filter(Q(birth_year__isnull=False)
                | Q(birth_place__isnull=False)
                | Q(death_year__isnull=False)
                | Q(notes__isnull=False),
                slug__isnull=False)
        .select_related('birth_place')
        .prefetch_related('texts', 'texts_as_editor', 'texts_as_storyteller')
    )
    ordering = ('last_name', 'first_name', 'name')


class PersonDetailView(generic.ListView):
    template_name = 'person_detail.html'
    model = Person
