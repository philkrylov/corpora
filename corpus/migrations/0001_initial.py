# Generated by Django 3.2.13 on 2023-08-21 11:15

import corpus.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AffixFeature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('signature', models.CharField(max_length=32, unique=True, verbose_name='Signature')),
                ('name_en', models.CharField(blank=True, max_length=64, null=True, verbose_name='Name (English)')),
                ('name_ru', models.CharField(blank=True, max_length=64, null=True, verbose_name='Name (Russian)')),
            ],
            options={
                'ordering': ['signature'],
            },
        ),
        migrations.CreateModel(
            name='Dialect',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_ru', models.CharField(max_length=128, verbose_name='Name (Russian)')),
                ('name_en', models.CharField(blank=True, max_length=128, null=True, verbose_name='Name (English)')),
            ],
        ),
        migrations.CreateModel(
            name='DictionaryEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('raw_text', models.TextField(blank=True, null=True, verbose_name='Исходная словарная статья')),
                ('lexeme', models.CharField(db_index=True, max_length=256, verbose_name='Лексема')),
                ('lexeme_no', models.CharField(blank=True, choices=[('', ''), ('I', 'I'), ('II', 'II'), ('III', 'III'), ('IV', 'IV'), ('V', 'V'), ('VI', 'VI'), ('VII', 'VII'), ('VIII', 'VIII'), ('IX', 'IX'), ('X', 'X')], max_length=5, null=True, verbose_name='Номер омонима')),
                ('raw_stem', models.CharField(blank=True, max_length=256, null=True, verbose_name='Основа глагола (по словарю)')),
                ('stem', models.CharField(blank=True, max_length=256, null=True, verbose_name='Основа имени')),
                ('form', models.CharField(blank=True, max_length=256, null=True, verbose_name='Словоформа со стяжением')),
                ('deriv', models.CharField(blank=True, max_length=256, null=True, verbose_name='Словообразовательные элементы')),
                ('derivgloss', models.CharField(blank=True, max_length=256, null=True, verbose_name='Словообразовательная глоссировка')),
                ('semgloss', models.CharField(blank=True, max_length=256, null=True, verbose_name='Перевод')),
                ('part', models.CharField(blank=True, choices=[('', ''), ('NOMEN', 'имя'), ('VERBUM', 'глагол'), ('союз', 'союз'), ('межд.', 'междометие'), ('нареч.', 'наречие'), ('част.', 'частица')], max_length=16, null=True, verbose_name='Часть речи')),
                ('phraseology', models.TextField(blank=True, null=True, verbose_name='Фразеология')),
                ('frequency', models.PositiveIntegerField(db_index=True, default=0, verbose_name='Частотность')),
            ],
            options={
                'verbose_name_plural': 'dictionary entries',
                'ordering': ['lexeme', 'lexeme_no'],
            },
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_ru', models.CharField(max_length=128, verbose_name='Name (Russian)')),
                ('name_en', models.CharField(blank=True, max_length=128, null=True, verbose_name='Name (English)')),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('first_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='First name')),
                ('last_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Last name')),
                ('birth_year', models.IntegerField(blank=True, null=True, verbose_name='Birth year')),
                ('death_year', models.IntegerField(blank=True, null=True, verbose_name='Death year')),
                ('notes', models.TextField(blank=True, null=True, verbose_name='Notes')),
                ('slug', models.SlugField(blank=True, null=True, verbose_name='Slug')),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_ru', models.CharField(max_length=128, verbose_name='Name (Russian)')),
                ('name_en', models.CharField(blank=True, max_length=128, null=True, verbose_name='Name (English)')),
            ],
        ),
        migrations.CreateModel(
            name='Publication',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=500, verbose_name='Title')),
                ('city', models.CharField(blank=True, max_length=64, null=True, verbose_name='Publication city')),
                ('publishing_house', models.CharField(blank=True, max_length=500, null=True, verbose_name='Publishing house')),
                ('year', models.IntegerField(blank=True, null=True, verbose_name='Publication year')),
                ('series', models.CharField(blank=True, max_length=500, null=True, verbose_name='Series')),
                ('volume', models.CharField(blank=True, max_length=64, null=True, verbose_name='Volume')),
                ('authors', models.ManyToManyField(blank=True, related_name='publications_as_author', to='corpus.Person', verbose_name='Authors')),
                ('compilers', models.ManyToManyField(blank=True, related_name='publications_as_compiler', to='corpus.Person', verbose_name='Compilers')),
                ('editors', models.ManyToManyField(blank=True, related_name='publications_as_editor', to='corpus.Person', verbose_name='Editors')),
            ],
            options={
                'ordering': ['year', 'title'],
            },
        ),
        migrations.CreateModel(
            name='Text',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=500, verbose_name='Title')),
                ('creation_time', models.CharField(blank=True, max_length=128, null=True, verbose_name='Time of writing')),
                ('city', models.CharField(blank=True, help_text='Устарело, перенести в Publication', max_length=64, null=True, verbose_name='Publication city')),
                ('year', models.IntegerField(blank=True, help_text='Устарело, перенести в Publication', null=True, verbose_name='Publication year')),
                ('n_word_tokens', models.PositiveIntegerField(blank=True, null=True, verbose_name='Word tokens')),
                ('n_alpha_words', models.PositiveIntegerField(blank=True, null=True, verbose_name='Letter word tokens')),
                ('n_phrases', models.PositiveIntegerField(blank=True, null=True, verbose_name='Phrases')),
                ('translator', models.CharField(blank=True, max_length=64, null=True, verbose_name='Translator')),
                ('translation_title', models.CharField(blank=True, max_length=500, null=True, verbose_name='Translation title')),
                ('translation_city', models.CharField(blank=True, help_text='Устарело, перенести в Translation publication', max_length=64, null=True, verbose_name='Translation publication city')),
                ('translation_year', models.PositiveIntegerField(blank=True, help_text='Устарело, перенести в Translation publication', null=True, verbose_name='Translation publication year')),
                ('datafile', models.FileField(blank=True, max_length=512, null=True, upload_to=corpus.models.UploadTo('textdata/'), verbose_name='Data file')),
                ('filetype', models.CharField(blank=True, choices=[('docx', '.docx: (source, translation)'), ('docx_3', '.docx: (reference, source, translation)'), ('docx_p', '.docx: (phonetic, source, translation)'), ('docx_4', '.docx: (reference, phonetic, source, translation)')], max_length=6, null=True, verbose_name='Data file type')),
                ('uploaded', models.DateTimeField(null=True, verbose_name='Upload date')),
                ('tokenized', models.DateTimeField(null=True, verbose_name='Tokenization date')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='texts', to='corpus.person', verbose_name='Author')),
                ('collector', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='texts_as_collector', to='corpus.person', verbose_name='Collector')),
                ('dialect', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='texts', to='corpus.dialect', verbose_name='Dialect')),
                ('editor', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='texts_as_editor', to='corpus.person', verbose_name='Editor')),
                ('genre', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='texts', to='corpus.genre', verbose_name='Genre')),
                ('publication', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='texts', to='corpus.publication', verbose_name='Publication')),
                ('record_place', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='texts', to='corpus.place', verbose_name='Recording place')),
                ('storyteller', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='texts_as_storyteller', to='corpus.person', verbose_name='Storyteller')),
                ('translation_publication', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='texts_as_translation', to='corpus.publication', verbose_name='Translation publication')),
            ],
            options={
                'ordering': ['author__name', 'year', 'title'],
            },
        ),
        migrations.CreateModel(
            name='TextSentence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('paragraph_number', models.PositiveIntegerField(verbose_name='Paragraph number within text')),
                ('number', models.PositiveIntegerField(verbose_name='Sentence number within paragraph')),
                ('source_phonetic', models.TextField(blank=True, null=True, verbose_name='Source sentence (phonetic representation)')),
                ('translation_en', models.TextField(blank=True, null=True, verbose_name='English translation')),
                ('translation_ru', models.TextField(blank=True, null=True, verbose_name='Russian translation')),
                ('reference', models.TextField(null=True, verbose_name='Reference address')),
                ('text', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sentences', to='corpus.text', verbose_name='Text')),
            ],
        ),
        migrations.CreateModel(
            name='WordFeature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('affix', models.CharField(max_length=32, verbose_name='Affix')),
                ('feature', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='corpus.affixfeature', verbose_name='Feature')),
            ],
            options={
                'unique_together': {('affix', 'feature')},
            },
        ),
        migrations.CreateModel(
            name='Wordform',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('wordform', models.CharField(max_length=256, unique=True, verbose_name='Wordform')),
                ('frequency', models.PositiveIntegerField(db_index=True, default=0, verbose_name='Frequency')),
                ('parsed', models.DateTimeField(blank=True, db_index=True, editable=False, null=True, verbose_name='Parse date')),
                ('n_auto_grammars', models.PositiveIntegerField(db_index=True, default=0, verbose_name='Analyses count')),
            ],
        ),
        migrations.CreateModel(
            name='WordGrammar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('auto', models.BooleanField(verbose_name='Automatic parse result')),
                ('key', models.CharField(db_index=True, max_length=512, verbose_name='Index key')),
                ('raw_stem', models.CharField(blank=True, max_length=256, null=True, verbose_name='Stem (if missing in dictionary)')),
                ('raw_semgloss', models.CharField(blank=True, max_length=256, null=True, verbose_name='Semantics (if missing in dictionary)')),
                ('weight', models.SmallIntegerField(verbose_name='Weight (feature count)')),
                ('lexeme', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='corpus.dictionaryentry', verbose_name='Lexeme')),
                ('wordform', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='grammars', to='corpus.wordform', verbose_name='Wordform')),
            ],
            options={
                'ordering': ('wordform', 'weight'),
                'index_together': {('wordform', 'weight')},
            },
        ),
        migrations.CreateModel(
            name='WordGrammarFeature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.IntegerField(db_index=True, verbose_name='Position within wordform')),
                ('word_feature', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='corpus.wordfeature', verbose_name='Affix and its feature')),
                ('word_grammar', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='features', to='corpus.wordgrammar', verbose_name='Wordform grammar')),
            ],
            options={
                'ordering': ('position',),
            },
        ),
        migrations.CreateModel(
            name='TextWord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveIntegerField(verbose_name='Word number within sentence')),
                ('clause', models.PositiveIntegerField(verbose_name='Clause')),
                ('leftpunct', models.CharField(blank=True, max_length=8, null=True, verbose_name='Left punctuation')),
                ('rightpunct', models.CharField(blank=True, max_length=8, null=True, verbose_name='Right punctuation')),
                ('confirmed_grammar', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='corpus.wordgrammar', verbose_name='Confirmed grammar info')),
                ('sentence', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='words', to='corpus.textsentence', verbose_name='Sentence')),
                ('wordform', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='corpus.wordform', verbose_name='Wordform')),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.AddField(
            model_name='person',
            name='birth_place',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='corpus.place', verbose_name='Birth place'),
        ),
    ]
