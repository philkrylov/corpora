from itertools import chain

from lxml import etree as ET

from suddenly import normalize


def xml_morpheme_iterator(morphemes):
    for m in morphemes:
        yield (normalize(m.find("./item[@type='txt']").text.strip()),
               m.find("./item[@type='gls']").text.strip())


def xml_word_iterator(sentence):
    words = sentence.findall("./words/word")
    if len(words) and (len(words) > 1 or
                       words[0].find("./item[@type='txt']").text is not None):
        word_seq = (
            (word_node.find("./item[@type='txt']").text.strip(),
             xml_morpheme_iterator(word_node.findall("./morphemes/morph")))
            for word_node in words
        )
    else:
        word_seq = (
            (w, None)
            for w in chain.from_iterable(
                t.strip().split(' ')
                for t in [sentence.find("./item[@type='txt']").text]
                if t is not None
            )
        )
    return word_seq


def xml_sentence_iterator(paragraph):
    for sentence in paragraph.findall('./phrases/phrase'):
        ru = sentence.find("./item[@type='gls']")
        yield (dict(translation_ru=(None if ru is None or ru.text is None
                                    else ru.text.strip())),
               xml_word_iterator(sentence))


def xml_paragraph_iterator(xml_file):
    root = ET.XML(xml_file.read())
    paragraphs = root.findall('./interlinear-text/paragraphs/paragraph')
    for paragraph in paragraphs:
        yield xml_sentence_iterator(paragraph)
