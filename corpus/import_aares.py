def aares_word_iterator(words):
    return ((w, None) for w in words)


def aares_sentence_iterator(lines, src_pos, trans_pos):
    while src_pos < len(lines):
        src = lines[src_pos]
        while src.startswith(b"p"):
            src_pos += 1
            src = lines[src_pos]
        while src.startswith((b"a", b"s")):
            src = src[1:]
        if src.startswith(b"S"):
            break
        if src.startswith(b"V"):
            src = src[1:].replace(rb'\r', b'').replace(br'\n', b'') \
                .decode('unicode-escape')
            src_pos += 1
        else:
            raise Exception("cannot parse %s" % src)
        trans = lines[trans_pos]
        while trans.startswith(b"p"):
            trans_pos += 1
            trans = lines[trans_pos]
        while trans.startswith((b"a", b"s")):
            trans = trans[1:]
        if trans.startswith(b"S"):
            trans = None
        elif trans.startswith(b"V"):
            trans = trans[1:].replace(rb'\r', b'').replace(rb'\n', b'') \
                .decode('unicode-escape')
            trans_pos += 1
        else:
            raise Exception("cannot parse %s" % trans)

        if trans is None:
            break
        yield (dict(translation_ru=trans),
               aares_word_iterator(src.strip().split(' ')))


def aares_paragraph_iterator(aares_file):
    lines = aares_file.readlines()
    state = 0
    for n, line in enumerate(lines):
        while line.startswith((b"a", b"s")):
            line = line[1:]
        if state == 0 and line.startswith(b"S'source_segs'"):
            state = 1
        elif state == 1 and line.startswith(b'V'):
            state = 2
            src_pos = n
        elif state == 2 and line.startswith(b"S'trans_segs'"):
            state = 3
        elif state == 3 and line.startswith(b'V'):
            trans_pos = n
            break
    yield aares_sentence_iterator(lines, src_pos, trans_pos)
