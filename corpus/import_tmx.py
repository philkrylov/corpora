from lxml import etree


def tmx_word_iterator(segments):
    for segment in segments:
        if segment.text:
            for w in segment.strip().split(' '):
                yield (w, None)


def tmx_sentence_iterator(sentences):
    for sentence in sentences:
        srclang = sentence.get('srclang')
        yield (dict(translation_ru=sentence.find("./tuv[srclang='%s']/seg" % srclang).text),
               tmx_word_iterator(sentence.xpath("tuv[@srclang != '%s']/seg")))


def tmx_paragraph_iterator(tmx_file):
    root = etree.XML(tmx_file.read())
    sentences = root.findall('./body/tu')
    yield tmx_sentence_iterator(sentences)
