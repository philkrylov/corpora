from functools import lru_cache
import logging
import re

from .models import DictionaryEntry


logger = logging.getLogger(__name__)


def _get_lexemes(**kwargs):
    return set(DictionaryEntry.objects.filter(**kwargs).values_list('lexeme',
                                                                    flat=True))


# Cache "special" lexemes.
# FIXME: this means the process must be restarted on dictionary updates!
hyphen_prefixed = _get_lexemes(lexeme__startswith='-')
hyphen_suffixed = _get_lexemes(lexeme__endswith='-')
bigram_starts = set(_.split(' ')[0]
                    for _ in _get_lexemes(lexeme__contains=' '))


def split_punct(word):
    """
    After space-splitting, split a resulting word returning a triple:
    (left_punctuation, word_stripped_of_punctuation, right_punctuation)
    """
    left_punct = ''
    right_punct = ''
    while word and not (word[:1].isalnum() or word[:1] == '-'):
        left_punct += word[:1]
        word = word[1:]
    while word and not (word[-1:].isalnum() or word[-1:] == '-'):
        right_punct = word[-1:] + right_punct
        word = word[:-1]
    if not word:
        return None, left_punct, None
    return (left_punct if left_punct else None,
            word,
            right_punct if right_punct else None)


def dummy_is_alphabetic(s):
    """
    A real implementation should only consider the given language's alphabet
    """
    return any(isalpha(c) for c in s)


def dummy_is_parseable(s):
    """A real implementation should return True if s is a parseable wordform"""
    return False


def dummy_normalize(s, lower=True):
    """A real implementation should fix common encoding/typo errors"""
    return s.lower() if lower else s


@lru_cache(maxsize=2048)
def normalize_punct(s, normalize=dummy_normalize):
    """Strip punctuation, call normalize, memoize result"""
    _, s, _ = split_punct(s)
    return normalize(s)


LEFT_PUNCTUATION = r'(\[{«‘‚‛“„‟'
RIGHT_PUNCTUATION = r')\]}»’”;,\.\!\?'
BOTH_PUNCTUATION = r'–—'


def split_words(s, is_parseable=dummy_is_parseable, normalize=dummy_normalize):
    """
    Tokenize string s, yielding successive tokens.
    To support bigram tokens and hyphen-joined wordforms, an is_parseable()
    implementation for the given language must be supplied.
    To fix common encoding errors, a normalize() implementation for the given
    language must be supplied.
    """
    logger.debug("split_words(%s)", s)
    sub = re.sub
    s = sub(r'([' + LEFT_PUNCTUATION + r']+)\s+', r'\1', s, flags=re.I + re.U)
    s = sub(r'\s+([' + RIGHT_PUNCTUATION + r']+)', r'\1', s, flags=re.I + re.U)
    words = s.strip().split()
    logger.debug("split_words: %s", words)
    skip = 0

    for n, word in enumerate(words):
        if skip:
            skip -= 1
            continue

        if re.search(r'[-\w][^-\w]', normalize_punct(word, normalize),
                     flags=re.I + re.U):
            word = sub(r'([' + LEFT_PUNCTUATION + r']+)', r' \1', word,
                       flags=re.I + re.U)
            word = sub(r'([^-\w' + LEFT_PUNCTUATION + r']+)', r'\1 ', word,
                       flags=re.I + re.U)
            word = sub(r'\s*([' + BOTH_PUNCTUATION + r']+)\s*', r' \1 ', word,
                       flags=re.I + re.U)
            logger.debug("resplitting word %s", word)
            for token in split_words(word, is_parseable):
                yield token
            continue

        if '-' in word:
            if is_parseable(word):
                tokens = [word]
            else:
                l, r = word.split('-', maxsplit=1)
                if normalize_punct(l, normalize) + '-' in hyphen_suffixed:
                    tokens = [l + '-', r]
                elif '-' + normalize_punct(r, normalize) in hyphen_prefixed:
                    tokens = [l, '-' + r]
                elif is_parseable(l) and is_parseable(r):
                    tokens = [l, '-', r]
                else:
                    tokens = [word]
            logger.debug("yielding %s for %s", tokens, word)
            for token in tokens:
                yield token

        elif n + 1 < len(words) \
                and normalize_punct(word, normalize) in bigram_starts:
            bigram = ' '.join((word, words[n + 1]))
            logger.debug("trying bigram %s", bigram)
            if is_parseable(bigram):
                logger.debug("bigram %s suceeded, skipping 1 word", bigram)
                skip = 1
                yield bigram
            else:
                logger.debug("bigram %s failed, yielding %s", bigram, word)
                yield word
        else:
            yield word
