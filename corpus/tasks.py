import importlib
import logging

from background_task import background

from django.conf import settings
from django import db

# Can't use relative imports for UWSGI spooler to work
from corpus.importers import IMPORTERS, import_from_iterator  # noqa: E402
from corpus.models import Text


logger = logging.getLogger('corpus.tasks')  # Can't use __name__ in spooler

language_module = importlib.import_module(settings.CORPUS_LANGUAGE_MODULE)


def retokenize_one(text, normalize, is_alphabetic):
    if text.datafile and text.filetype in IMPORTERS:
        text.datafile.open(mode="rb")
        import_from_iterator(text, IMPORTERS[text.filetype](text.datafile),
                             normalize=normalize, is_alphabetic=is_alphabetic)
        text.datafile.close()


@background()
def retokenize(arguments):
    db.connections.close_all()
    ids = arguments['ids']
    logger.debug("==== tokenization task: text ids: %s", ids)

    for text in Text.objects.filter(id__in=ids):
        retokenize_one(text, language_module.normalize,
                       language_module.is_alphabetic)
    with db.connection.cursor() as cursor:
        cursor.execute("""
            UPDATE corpus_wordform w
            SET frequency = (SELECT COUNT(*) FROM corpus_textword
                             WHERE wordform_id = w.id)
        """)
