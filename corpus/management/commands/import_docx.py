import os.path
import re

from django.core.management.base import BaseCommand, CommandError
from django.core.files import File
from django.utils import timezone

from corpus.models import Text


class Command(BaseCommand):
    help = 'Import 2-column docx files'

    def add_arguments(self, parser):
        parser.add_argument('filenames', nargs='+')

    def handle(self, *args, **options):
        for filename in options['filenames']:
            self.stdout.write(repr(filename) + "\n")
            id = int(re.match(r'(\d+)_.*',
                              os.path.basename(filename)).group(1))
            try:
                text = Text.objects.get(pk=id)
            except Text.DoesNotExist:
                raise CommandError('Text "%s" does not exist' % id)
            with open(filename, "rb") as f:
                text.datafile.save(os.path.basename(filename), File(f))
            text.filetype = 'docx'
            text.uploaded = timezone.now()
            text.n_phrases = None
            text.n_word_tokens = None
            text.tokenized = None
            text.save()
            self.stdout.write(self.style.SUCCESS(
                'Successfully imported file "%s" as text %d "%s"' %
                (filename, id, text.title)
            ))
