# -*- coding: utf-8 -*-

import os.path
import re

from lxml import etree as ET

from django.conf import settings
from django.views import generic

from .models import (
        AudioText,
        EafText,
        GlossedText,
)


RE_CHARACTERISTIC = re.compile(r'[a-zA-Z0-9.,=+)(]')


class EafDetailView(generic.DetailView):
    template_name = 'eaf_detail.html'
    model = EafText

    def get_context_data(self, **kwargs):
        name = os.path.join(settings.BASE_DIR, str(self.object.storage))
        tree = ET.parse(name)
        root = tree.getroot()
        sentences = root.findall("./TIER[@TIER_ID='KH_Sent']/ANNOTATION/"
                                 "ALIGNABLE_ANNOTATION")
        ru_sentences = root.find("./TIER[@TIER_ID='RUS_Sent']")
        words = root.find("./TIER[@TIER_ID='KH_Words']")
        # ru_words = root.find("./TIER[@TIER_ID='RUS_Words']")
        homonyms = root.find("./TIER[@TIER_ID='KH_Homonyms']")
        ru_homonyms = root.find("./TIER[@TIER_ID='Rus_Homonyms']")

        ctx_sentences = []
        for sentence in sentences:
            id = sentence.attrib['ANNOTATION_ID']
            ts1 = int(sentence.attrib['TIME_SLOT_REF1'][2:])
            ts2 = int(sentence.attrib['TIME_SLOT_REF2'][2:])
            ctx_sent = {
                'kh': sentence.find("./ANNOTATION_VALUE").text,
                'ru': ru_sentences.find("./ANNOTATION/REF_ANNOTATION"
                                        "[@ANNOTATION_REF='%s']/"
                                        "ANNOTATION_VALUE" % id).text,
            }
            ctx_words = []
            for ts in ('ts%d' % ts_id for ts_id in range(ts1, ts2)):
                word = words.find("./ANNOTATION/ALIGNABLE_ANNOTATION"
                                  "[@TIME_SLOT_REF1='%s']" % ts)
                if not len(word):
                    continue
                # word_id = word.attrib['ANNOTATION_ID']
                wts1 = int(word.attrib['TIME_SLOT_REF1'][2:])
                wts2 = int(word.attrib['TIME_SLOT_REF2'][2:])
                ctx_word = {
                    'kh': word.find("./ANNOTATION_VALUE").text,
                    # 'ru': ru_words.find("./ANNOTATION/REF_ANNOTATION"
                    #                     "[@ANNOTATION_REF='%s']/"
                    #                     "ANNOTATION_VALUE" % word_id).text,
                }
                ctx_homonyms = []
                for wts in ('ts%d' % ts_id for ts_id in range(wts1, wts2)):
                    homonym = homonyms.find("./ANNOTATION/ALIGNABLE_ANNOTATION"
                                            "[@TIME_SLOT_REF1='%s']" % wts)
                    if not len(homonym):
                        continue
                    homonym_id = homonym.attrib['ANNOTATION_ID']
                    kh_homonym = \
                        homonym.find("./ANNOTATION_VALUE").text.split('-')
                    ru_homonym = ru_homonyms.find("./ANNOTATION/REF_ANNOTATION"
                                                  "[@ANNOTATION_REF='%s']/"
                                                  "ANNOTATION_VALUE" %
                                                  homonym_id).text.split('-')
                    ctx_homonym = [
                        {'kh': kh_homonym[n],
                         'ru': ru_homonym[n],
                         'ru_is_characteristic':
                             bool(RE_CHARACTERISTIC.match(ru_homonym[n]))}
                        for n in range(len(kh_homonym))
                    ]
                    ctx_homonyms.append(ctx_homonym)
                ctx_word['homonyms'] = ctx_homonyms
                ctx_words.append(ctx_word)
            ctx_sent['words'] = ctx_words
            ctx_sentences.append(ctx_sent)
        return {
            'object': self.object,
            'sentences': ctx_sentences,
        }


class EafView(generic.ListView):
    template_name = 'eaf.html'
    model = EafText


class AudioTextsView(generic.ListView):
    template_name = 'audiotexts.html'
    model = AudioText


class TextDetailView(generic.DetailView):
    template_name = 'text_detail.html'
    model = GlossedText
