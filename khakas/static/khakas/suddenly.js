var normalize = (function() {
  var translate_re = /[Ӏӏaceiӊopxyöÿҷ]/g;
  var translate = {
    '\u04C0': '\u0406',
    '\u04CF': '\u0456',
    'a': 'а',
    'c': 'с',
    'e': 'е',
    'i': '\u0456',
    'ӊ': 'ң',
    'o': 'о',
    'p': 'р',
    'x': 'х',
    'y': 'у',
    'ö': '\u04E7',
    'ÿ': '\u04F1',
    'ҷ': 'ӌ'
  };
  return function(s) {
    return s.toLowerCase().replace(translate_re, function(match) {
      return translate[match];
    });
  }
})();


var n_queue = 0;

function parse_word(word, callback_data, callback) {
  n_queue = (n_queue + 1) % 8;
  $.ajaxq('q' + n_queue, {context: [callback, callback_data],
                          data: 'parse=' + word,
                          url: '/suddenly/',
                          success: function(data, status, xhr) {
    var homonyms = [];
    var i, j, k, m;
    while ((i = data.indexOf('FOUND STEM:')) >= 0) {
      var homonym = {};
      data = data.substr(i + 12);
      j = data.indexOf(' ');
      i = data.indexOf('\n');
      homonym['form'] = data.substr(0, j);
      k = data.indexOf(' ', j + 1);
      if (k >= 0 && k < i && (m = data.indexOf(' ', k + 1)) >= 0 && m < i) {
        homonym['affixes'] = data.substring(j + 1, k);
        homonym['prefixform'] = data.substring(k + 1, m);
        homonym['prefixes'] = data.substring(m + 1, i);
      } else {
        homonym['affixes'] = data.substring(j + 1, i);
      }
      data = data.substr(i + 1);
      homonym['p_o_s'] = data[0];
      i = data.indexOf('\n');
      var dict = data.substring(2, i);
      j = dict.indexOf(' ‛');
      homonym['headword'] = dict.substr(0, j);
      k = dict.indexOf('’');
      homonym['meaning'] = dict.substring(j + 1, k + 1);
      if (dict.length > k + 1) {
        var rest = dict.substr(k + 2).trim();
        k = rest.lastIndexOf(' ');
        if (k >= 0) {
          homonym['stem'] = rest.substr(0, k);
          homonym['id'] = parseInt(rest.substr(k + 1));
        } else {
          homonym['id'] = parseInt(rest);
        }
      }
      homonyms.push(homonym);
    }
    this[0](this[1], homonyms);
  } });
}
