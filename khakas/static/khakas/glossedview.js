function load_view(name, data) {
  var xml;
  if (typeof(data) == "string") {
    xml = $.parseXML(data);
    text[name] = xml;
  } else xml = data;
  var view = $('#glossed-view');
  view.html('');
  var phrase_id = 0;
  $(xml).find('paragraph').each(function() {
    var p = $('<p/>');
    view.append(p);
    $(this).find('phrase').each(function() {
      var phrase_id_node = $(this).find('item[type=ref]').eq(0).text().substr(1);
      if (phrase_id_node.length) {
        phrase_id = parseInt(phrase_id_node);
      } else {
        phrase_id++;
      }
      var phrase = $('<div class="phrase" id="phr' + phrase_id + '"><div class="gls">' +
                     $(this).find('item[type=gls]').eq(0).text() +
                     '</div></div>');
      var words = $('<div class="words">');
      var phrase_txt = '';
      var any_words = false;
      phrase.prepend(words);
      p.append(phrase);
      $(this).find('word').each(function() {
        any_words = true;
        var word_text = $(this).find('item[type=txt]').eq(0).text();
        phrase_txt += word_text + ' ';
        var word = $('<div class="word"><div class="txt">' + word_text + '</div></div>');
        words.append(word);
        var n_homonyms = $(this).find('morph item[type=txt]').eq(0).text().replace(/@\s+$/, '').split('@').length;
        if (n_homonyms > 1) {
          var homonymy = $('<div class="homonymy">');
          word.append(homonymy);
          for (var i = 0; i < n_homonyms; i++) {
            homonymy.append($('<div class="morphemes"><span class="omonum">' + (i + 1) + '</span></div>'));
          }
        } else {
          word.append($('<div class="morphemes"/>'));
        }
        $(this).find('morph').each(function() {
          var alt_txt = $(this).find('item[type=txt]').text().split('@');
          var alt_gls = $(this).find('item[type=gls]').text().replace(/([А-Яа-я])i/g, '$1і').replace(/i([А-Яа-я])/g, 'і$1').replace(/([a-zA-Z0-9.,=+)(]+)/g, '<span class="sc">$1</span>').split('@');
          $.each(alt_txt, function(i, txt) {
            $('.morphemes', word).eq(i).append('<div class="morpheme"><div class="mtxt">' + txt + '</div><div class="mgls">' + alt_gls[i] + '</div></div>');
            var mgls = $('.morpheme:last .mgls', $('.morphemes', word).eq(i));
            if (mgls.text().length > 25) {
              mgls.html(mgls.html().substr(0, 20) + '...')
            }
          });
        });
      });
      var phrase_node = $(this).find('item[type=txt]').eq(0);
      if (phrase_node) {
        phrase_txt = phrase_node.text();
      }
      phrase.prepend('<div class="txt">' + phrase_txt + '</div>');
    });
  });
}
