    function group_by(arr, key) {
      var grouped = {};
      arr.forEach(function(obj) {
        var parts = key.split('.');
        var part;
        var ptr = obj;
        while (part = parts.shift()) {
          ptr = ptr[part];
        }
        if (!grouped[ptr]) {
          grouped[ptr] = {group: ptr, items: []};
        }
        grouped[ptr].items.push(obj);
      });
      return grouped;
    }

$(function(){
});

    function update_formula(word_filter) {
      var features = $('ul.features_auto', word_filter);
      var p = features.prev();
      $('.features_formula', p).remove();
      var ss = [];
      $('.slotno', word_filter).each(function() {
        var slot_name = $(this).text()
        var slot_no = parseInt(this.className.match(/slotno-(-?\d+)/)[1]);
        var s1 = [];
        $('input.slot-' + slot_no + ':checked', word_filter).each(function() {
          s1.push({'name': $('span:first', this.parentElement).html(),
                   'affix': $('span:last', this.parentElement).html()});
        });
        $('li.newslot label input[value=' + slot_no + ']:checked', word_filter).each(function() {
          s1.push({'name': slot_name, 'affix': '∅'});
        });
        if (s1.length) {
          ss.push(s1);
        }
      });
      p.append(formula_tmpl({slots: ss}));
      p.unbind('click').click(function() {
        if (features.is(':hidden')) {
          $('.features_auto').hide();
          features.show();
          $('input', features).show();
	  p.addClass('zoomout'); p.removeClass('zoomin');
        } else {
          $('.features_auto').show();
          features.hide();
          $('input', features).hide();
	  p.addClass('zoomin'); p.removeClass('zoomout');
        }
      });
    }

    function update_slot_checkboxes(word_filter) {
      $('.slotno', word_filter).each(function() {
        var slot_no = parseInt(this.className.match(/slotno-(-?\d+)/)[1]);
        $('input', this).prop('checked',
                              ($('input.slot-' + slot_no, word_filter).length ==
                               $('input.slot-' + slot_no + ':checked', word_filter).length));
        $('input', this).attr('onchange', 'update_formula(this.parentElement.parentElement.parentElement)');
      });
    }

    function empty_change(checkbox) {
      var word_filter = $(checkbox).parents('.textword_filter')[0];
      if (checkbox.checked) {
        var n_slot = checkbox.value;
        var ul = $(checkbox).parents('.features_auto')
        $('input.slot-' + n_slot, ul).prop('checked', false);
        update_slot_checkboxes(word_filter);
      }
      update_formula(word_filter);
    }

    function affix_click(checkbox, n_slot) {
      var word_filter = $(checkbox).parents('.textword_filter')[0];
      $('.newslot input[value=' + n_slot + ']', word_filter).prop('checked', false);
      update_slot_checkboxes(word_filter);
    }

    function slot_click(checkbox, n_slot) {
      var word_filter = $(checkbox).parents('.textword_filter')[0];
      var ul = $(checkbox).parents('.features_auto');
      $('.newslot input[value=' + n_slot + ']', ul).prop('checked', false);
      $('input.slot-' + n_slot, ul).prop('checked', checkbox.checked);
      update_formula(word_filter);
    }

    function add_slots(word_filter) {
      function get_input_selector(affix_id) {
        return $('.features_auto li[class!=newslot] input[value=' + affix_id + ']', word_filter);
      }
      for (var n = 0; n < positions.length; n++) {
        var n_slot = positions[n][1];
        var input_sel = get_input_selector(positions[n][0]);
        input_sel.addClass('slot-' + n_slot);
        if (n == 0 || n_slot != positions[n - 1][1]) {
          var li = input_sel.parent().parent();
          empty_checkbox = $('ul.empty_slots_auto input[value=' + n_slot + ']', word_filter);
          empty_checkbox.attr('onchange', 'empty_change(this)');
          li.before(empty_checkbox.parents('li'));
          li = li.prev()
          li.addClass('newslot');
          li.prepend(window.slot_caption_tmpl({n_slot: n_slot, slot_title: ('слот ' + n_slot).replace(/-/, '−')}));
        }
      }
      for (var n = 0; n < positions.length; n++) {
        var n_slot = positions[n][1];
        var input_sel = get_input_selector(positions[n][0]);
        $(input_sel).attr('onclick', 'affix_click(this, ' + n_slot + ')');
        $(input_sel).attr('onchange', 'update_formula($(this).parents(".textword_filter"))');
      }
      $('ul.empty_slots_auto', word_filter).add($('ul.empty_slots_auto', word_filter).prev()).remove();
      update_slot_checkboxes(word_filter);
    }

    function add_textword_filter(button) {
      var idx = $('.textword_filter').length + 1;
      $(button).before('<div class="textword_filter">' + wordfilter_tmpl.replace(/word1/g, 'word' + idx) + '</div>');
      $('.features_auto').hide();
      update_formula($('.textword_filter:last')[0]);
      install_charmap($('.textword_filter:last a.charmap-launch'));
    }

    function remove_textword_filter(button) {
      var filter = $(button).parents('.textword_filter');
      var idx = parseInt($('input.wordform', filter)[0].id.match(/\d+/)[0]);
      filter.remove();
      function update(e, attrs, n) {
        attrs.forEach(function(a) {
          var old = $(e).attr(a);
          if (old) {
            $(e).attr(a, old.replace(RegExp('word' + (idx + n + 1), 'g'),
                                     'word' + (idx + n)));
          }
        });
      }
      $('.textword_filter').slice(idx - 1).each(function(n, e) {
        $('label', e).each(function() { update(this, ['for'], n); });
        $('input', e).each(function() { update(this, ['id', 'name'], n); });
        $('select', e).each(function() { update(this, ['id', 'name'], n); });
        $('ul', e).each(function() { update(this, ['id'], n); });
      });
    }

    $(function(){
      window.formula_tmpl = Handlebars.compile(document.getElementById("formula-tmpl").innerHTML);
      window.slot_caption_tmpl = Handlebars.compile(document.getElementById("slot-caption-tmpl").innerHTML);
      $('.textword_filter').each(function(n, e) { add_slots(e); });
      window.wordfilter_tmpl = $('<box>' + $('.textword_filter').eq(0).html() + '</box>');
      $('input[type=checkbox]', wordfilter_tmpl).removeAttr('checked');
      wordfilter_tmpl = wordfilter_tmpl.html();
      $('.textword_filter').each(function(n, e) { update_formula(e); });
      $('.features_auto').hide();
    });


    $(function () {
      const reduceOp = function(args, reducer){
        args = Array.from(args);
        args.pop(); // => options
        var first = args.shift();
        return args.reduce(reducer, first);
      };

      Handlebars.registerHelper({
        eq  : function(){ return reduceOp(arguments, (a,b) => a === b); },
        ne  : function(){ return reduceOp(arguments, (a,b) => a !== b); },
        lt  : function(){ return reduceOp(arguments, (a,b) => a  <  b); },
        gt  : function(){ return reduceOp(arguments, (a,b) => a  >  b); },
        lte : function(){ return reduceOp(arguments, (a,b) => a  <= b); },
        gte : function(){ return reduceOp(arguments, (a,b) => a  >= b); },
        and : function(){ return reduceOp(arguments, (a,b) => a  && b); },
        or  : function(){ return reduceOp(arguments, (a,b) => a  || b); },
        sum : function(){ return reduceOp(arguments, (a,b) => a  +  b); },
        texts: function(idx, options) { return options.fn(texts[idx].items[0]); },
        highlight: function(id, ids, options) { console.log(id);console.log(ids);return ids.includes(id) ? ' highlight' : ''; },
        groupby: function(list, key, options) {
          var grouped = group_by(list, key)
          var result = "";
          Object.keys(grouped).forEach(function(k, index) {
            result += options.fn(grouped[k]);
          });
          return result;
        }
      });
      Handlebars.registerPartial('word_grammar', Handlebars.compile(document.getElementById("word_grammar-tmpl").innerHTML));
      var results_tmpl = Handlebars.compile(document.getElementById("results-tmpl").innerHTML);

      function url_get_query(url) {
        return url.replace(/^[^?]+\?/, '');
      }

      function show_result(data, textStatus, jqXHR) {
        var query = url_get_query(this.url);
        var new_url = '/corpus/?' + query;
        var naked_url = new_url.replace(/\b&?page=[0-9]+/, "");

        data.page = jqXHR._page;
        data.n_last_page = Math.ceil(data.n_sentence_matches / 500);
        data.first_page = (data.page >= 2) ? naked_url + '&page=1' : '';
        data.prev_page = (data.page >= 2) ? naked_url + '&page=' + (data.page - 1) : '';
        data.next_page = (data.page < data.n_last_page) ? naked_url + '&page=' + (data.page + 1) : '';
        data.last_page = (data.page < data.n_last_page) ? naked_url + '&page=' + data.n_last_page : '';

        document.getElementById("glossed-view").innerHTML = results_tmpl(data);
        history.pushState({"data": query}, "", new_url);
      }

      window.onpopstate = function(e) {
        document.getElementById("glossed-view").innerHTML = results_tmpl(e.state['data']);
        $.ajax(document.location.protocol + '//' + document.location.host + '/api/sentence/',
               {'data': e.state['data'],
                'success': function(data, textStatus, jqXHR) {
                  document.getElementById("glossed-view").innerHTML = results_tmpl(data);
                }});
      };

      window.submit_query = function(e) {
        $('.features_auto').hide();
        $('input[type=submit]').disabled = true;
        $('.wait').show("fast");
        $.ajax(document.location.protocol + '//' + document.location.host + '/api/sentence/',
               {'data': $('form').not('input[value=""]').serialize(),
                'dataType': "json",
                'accepts': { text: "application/json" },
                'beforeSend': function(jqXHR, settings) { jqXHR._page = parseInt($('#id_page').prop('value')); },
                'complete': function() { $('.wait').hide("fast");
                                         $('input[type=submit]')[0].disabled = false; },
                'error': function(jqXHR, textStatus, errorThrown) {
                  if (textStatus == "error" && errorThrown == "Not Found") {
                    $('#glossed-view').html('Соответствия не найдены.');
                  } else {
                    $('#glossed-view').html(textStatus + "<br/>" + errorThrown);
                  }
                },
                'success': show_result});
        e.preventDefault();
      }

      $('form').submit(submit_query);
    });
    function go_page(n_page) {
      $('#id_page').val(n_page);
      $('form').submit();
    }
