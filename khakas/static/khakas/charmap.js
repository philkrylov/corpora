/**
 * Original code from Alex Brem [jquery-fieldselection](https://github.com/localhost/jquery-fieldselection)
 */
(function($) {
 $.fn.replaceSelection = function() {
 var e = (this.jquery) ? this[0] : this;
 var text = arguments[0] || '';
 return (
     /* mozilla / dom 3.0 */
     ('selectionStart' in e && function() {
      e.value = e.value.substr(0, e.selectionStart) + text
      + e.value.substr(e.selectionEnd, e.value.length);
      return this;
      })
     ||
     /* explorer */
     (document.selection && function() {
      e.focus();
      document.selection.createRange().text = text;
      return this;
      }) ||
     /* browser not supported */
     function() {
     e.value += text;
     return jQuery(e);
     })();
 };
})(jQuery);

function install_charmap(selector) {
  selector.click(function(){
    if ($('.charmap-widget').length) {
      $('.charmap-widget').remove();
      $('.charmap-launch').removeClass('pressed');
      return false;
    }
    $(this).addClass('pressed');
    var layout = [
      ['й'], ['ц'], ['у', 'ӱ'], ['к'], ['е'], ['н', 'ң'], ['г', 'ғ'], ['ш'], ['щ'], ['з'], ['х'], ['ъ'], ['ё'], [],
      ['ф'], ['ы'], ['в'], ['а'], ['п'], ['р'], ['о', 'ӧ'], ['л'], ['д'], ['ж'], ['э'], [],
      ['я'], ['ч', 'ӌ'], ['с'], ['м'], ['и', 'і'], ['т'], ['ь'], ['б'], ['ю']
    ];
    $(this).after('<div class="charmap-widget"><div class="transparency"></div></div>');
    //$('.charmap-widget').css({'margin-left': -(550 - $(this).prev().width()) / 2});
    var i;
    for (i=0; i<layout.length; i++) {
      if (layout[i].length == 0) {
        $('.charmap-widget').append('<br/>');
        continue;
      }
      var row = '<div class="charmap-row">';
      for (j=0; j<layout[i].length; j++) {
        row += '<input type="button" value="' + layout[i][j] + '"></input>';
      }
      $('.charmap-widget').append(row);
    }
    $('.charmap-row input').click(function() {
      var target = $(this).parent().parent().prevAll('.charmap').slice(-1,1);
      target.focus();
      var c = $(this).val();
      target.replaceSelection(c);
      target.trigger("input");
    });
    return false;
  });
}

$(function(){
  $('.charmap').after('<a class="charmap-launch" href="#">\u2328</a>');//.parent().css({'white-space': 'nowrap', position: 'relative'});
  install_charmap($('a.charmap-launch'));
});
