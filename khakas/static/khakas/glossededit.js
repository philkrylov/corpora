function apply_edit_behaviors() {
  var panel = $('<div id="edit-panel"><ul>' +
                '<li><a href="#" onclick="autogloss(); return false;">автоматическое глоссирование</a></li>' +
                '<li class="h"><b>Перейти:</b></li>' +
                '<li><a href="#" onclick="go_first_unglossed(); return false;">первый неглоссированный сегмент</a></li>' +
                '<li><a href="#" onclick="go_first_homonymy(); return false;">первый сегмент с омонимией</a></li>' +
                '</ul></div>');
  $('body').prepend(panel);
  $('.tab-content').addClass('has-panel');
}

function phrase_activate(el) {
  $('.active-phrase').removeClass('active-phrase');
  el.scrollIntoView(true);
  $(el).addClass('active-phrase');
  if (phrase_is_unsplit(el)) {
    phrase_split(el);
  }
}

function phrase_autogloss(el) {
  $('.word', el).each(function() {
    var re = /[а-я\u0456\u04E7\u04F1\u0493\u04A3\u04CC]+/g;
    if (!$('.morphemes', this).length) {
      var normalized = re.exec(normalize($('.txt', this).text()));
      if (!normalized)
        return;
      parse_word(normalized[0], this, function(word, homonyms) {
        if (homonyms.length > 1) {
          var homonymy = $('<div class="homonymy">');
          $(word).append(homonymy);
          for (var i = 0; i < homonyms.length; i++) {
            homonymy.append($('<div class="morphemes"><span class="omonum">' + (i + 1) + '</span></div>'));
          }
        } else {
          $(word).append($('<div class="morphemes"/>'));
        }
        $.each(homonyms, function(i, homonym) {
          var $morphemes = $('.morphemes', word).eq(i);
          $morphemes.append('<div class="morpheme">' +
                            '<div class="mtxt">' + (homonym.stem || homonym.headword) + '</div>' +
                            '<div class="mgls">' + homonym.meaning + '</div>' +
                            '</div>');
          var formnames = homonym.form.split('-');
          var affixes = homonym.affixes.split('-');
          $.each(affixes, function(i) {
            if (affixes[i].length) {
              $morphemes.append('<div class="morpheme">' +
                                '<div class="mtxt">' + affixes[i] + '</div>' +
                                '<div class="mgls"><span class="sc">' + formnames[i] + '</span></div>' +
                                '</div>');
            }
          });
        });
      });
    }
  });
}

function phrase_split(el) {
  var words_list = $('.txt', el).text().split(/ +/);
  var words = $('.words', el);
  $.each(words_list, function(i, word_text) {
    var word = $('<div class="word"><div class="txt">' + word_text + '</div></div>');
    words.append(word);
  });
}

function phrase_is_unsplit(el) {
  if (!$('.words', el).get(0).children.length) {
    return true;
  }
}

function go_first_unglossed() {
  $('.phrase').each(function() {
    if (phrase_is_unsplit(this)) {
      phrase_activate(this);
      return false;
    }
  });
  return false;
}

function autogloss() {
  //phrase_autogloss($('.active-phrase').get(0));
  $('.phrase').each(function() {
    if (phrase_is_unsplit(this)) {
      phrase_activate(this);
    }
    phrase_autogloss(this);
  });
};
