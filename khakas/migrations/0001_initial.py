# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-19 21:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AudioText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=256, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('text_storage', models.FileField(blank=True, max_length=256, upload_to=b'audio', verbose_name='\u0422\u0435\u043a\u0441\u0442')),
                ('audio_storage', models.FileField(blank=True, max_length=256, upload_to=b'audio', verbose_name='Audio')),
            ],
        ),
        migrations.CreateModel(
            name='DictionaryEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('raw_text', models.TextField(blank=True, null=True, verbose_name='\u0418\u0441\u0445\u043e\u0434\u043d\u0430\u044f \u0441\u043b\u043e\u0432\u0430\u0440\u043d\u0430\u044f \u0441\u0442\u0430\u0442\u044c\u044f')),
                ('lexeme', models.CharField(db_index=True, max_length=256, verbose_name='\u041b\u0435\u043a\u0441\u0435\u043c\u0430')),
                ('lexeme_no', models.CharField(blank=True, choices=[(b'', b''), (b'I', b'I'), (b'II', b'II'), (b'III', b'III'), (b'IV', b'IV'), (b'V', b'V'), (b'VI', b'VI'), (b'VII', b'VII'), (b'VIII', b'VIII'), (b'IX', b'IX'), (b'X', b'X')], max_length=5, null=True, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u043e\u043c\u043e\u043d\u0438\u043c\u0430')),
                ('raw_stem', models.CharField(blank=True, max_length=256, null=True, verbose_name='\u041e\u0441\u043d\u043e\u0432\u0430 \u0433\u043b\u0430\u0433\u043e\u043b\u0430 (\u043f\u043e \u0441\u043b\u043e\u0432\u0430\u0440\u044e)')),
                ('stem', models.CharField(blank=True, max_length=256, null=True, verbose_name='\u041e\u0441\u043d\u043e\u0432\u0430 \u0438\u043c\u0435\u043d\u0438')),
                ('form', models.CharField(blank=True, max_length=256, null=True, verbose_name='\u0421\u043b\u043e\u0432\u043e\u0444\u043e\u0440\u043c\u0430 \u0441\u043e \u0441\u0442\u044f\u0436\u0435\u043d\u0438\u0435\u043c')),
                ('deriv', models.CharField(blank=True, max_length=256, null=True, verbose_name='\u0421\u043b\u043e\u0432\u043e\u043e\u0431\u0440\u0430\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0435 \u044d\u043b\u0435\u043c\u0435\u043d\u0442\u044b')),
                ('derivgloss', models.CharField(blank=True, max_length=256, null=True, verbose_name='\u0421\u043b\u043e\u0432\u043e\u043e\u0431\u0440\u0430\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c\u043d\u0430\u044f \u0433\u043b\u043e\u0441\u0441\u0438\u0440\u043e\u0432\u043a\u0430')),
                ('semgloss', models.CharField(blank=True, max_length=256, null=True, verbose_name='\u041f\u0435\u0440\u0435\u0432\u043e\u0434')),
                ('part', models.CharField(blank=True, choices=[(b'', b''), (b'NOMEN', b'\xd0\xb8\xd0\xbc\xd1\x8f'), (b'VERBUM', b'\xd0\xb3\xd0\xbb\xd0\xb0\xd0\xb3\xd0\xbe\xd0\xbb'), ('\u0441\u043e\u044e\u0437', b'\xd1\x81\xd0\xbe\xd1\x8e\xd0\xb7'), ('\u043c\u0435\u0436\u0434.', b'\xd0\xbc\xd0\xb5\xd0\xb6\xd0\xb4\xd0\xbe\xd0\xbc\xd0\xb5\xd1\x82\xd0\xb8\xd0\xb5'), ('\u043d\u0430\u0440\u0435\u0447.', b'\xd0\xbd\xd0\xb0\xd1\x80\xd0\xb5\xd1\x87\xd0\xb8\xd0\xb5'), ('\u0447\u0430\u0441\u0442.', b'\xd1\x87\xd0\xb0\xd1\x81\xd1\x82\xd0\xb8\xd1\x86\xd0\xb0')], max_length=16, null=True, verbose_name='\u0427\u0430\u0441\u0442\u044c \u0440\u0435\u0447\u0438')),
                ('phraseology', models.TextField(blank=True, null=True, verbose_name='\u0424\u0440\u0430\u0437\u0435\u043e\u043b\u043e\u0433\u0438\u044f')),
            ],
            options={
                'ordering': ['lexeme', 'lexeme_no'],
                'verbose_name_plural': 'dictionary entries',
            },
        ),
        migrations.CreateModel(
            name='EafText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=256, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('storage', models.FileField(max_length=256, upload_to=b'eaf', verbose_name='\u0424\u0430\u0439\u043b EAF')),
            ],
        ),
        migrations.CreateModel(
            name='GlossedText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=256, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
                ('genre', models.CharField(max_length=256, verbose_name='\u0416\u0430\u043d\u0440')),
                ('author', models.CharField(max_length=256, verbose_name='\u0410\u0432\u0442\u043e\u0440')),
                ('creation_date', models.CharField(max_length=256, verbose_name='\u0412\u0440\u0435\u043c\u044f \u043d\u0430\u043f\u0438\u0441\u0430\u043d\u0438\u044f')),
                ('quant_features', models.CharField(max_length=256, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0435 \u0445\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438')),
                ('source_biblio', models.CharField(max_length=256, verbose_name='\u0411\u0438\u0431\u043b\u0438\u043e\u0433\u0440\u0430\u0444\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u0434\u0430\u043d\u043d\u044b\u0435 \u0438\u0441\u0442\u043e\u0447\u043d\u0438\u043a\u0430')),
                ('translator', models.CharField(blank=True, max_length=256, verbose_name='\u041f\u0435\u0440\u0435\u0432\u043e\u0434\u0447\u0438\u043a')),
                ('translation_biblio', models.CharField(blank=True, max_length=256, verbose_name='\u0411\u0438\u0431\u043b\u0438\u043e\u0433\u0440\u0430\u0444\u0438\u0447\u0435\u0441\u043a\u0438\u0435 \u0434\u0430\u043d\u043d\u044b\u0435 \u043f\u0435\u0440\u0435\u0432\u043e\u0434\u0430')),
                ('storage', models.FileField(blank=True, max_length=256, upload_to=b'texts', verbose_name='\u0424\u0430\u0439\u043b')),
            ],
        ),
    ]
