# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-12-25 21:03
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('khakas', '0007_TextAuthor_add_birth_year_birth_place'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='textauthor',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='textsentence',
            name='reference',
            field=models.CharField(max_length=64, null=True, verbose_name='Reference address'),
        ),
        migrations.AlterField(
            model_name='affixfeature',
            name='name_en',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='Name (English)'),
        ),
        migrations.AlterField(
            model_name='affixfeature',
            name='name_ru',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='Name (Russian)'),
        ),
        migrations.AlterField(
            model_name='affixfeature',
            name='signature',
            field=models.CharField(max_length=32, unique=True, verbose_name='Signature'),
        ),
        migrations.AlterField(
            model_name='audiotext',
            name='audio_storage',
            field=models.FileField(blank=True, max_length=256, upload_to='audio', verbose_name='Audio'),
        ),
        migrations.AlterField(
            model_name='audiotext',
            name='text_storage',
            field=models.FileField(blank=True, max_length=256, upload_to='audio', verbose_name='Текст'),
        ),
        migrations.AlterField(
            model_name='dictionaryentry',
            name='lexeme_no',
            field=models.CharField(blank=True, choices=[('', ''), ('I', 'I'), ('II', 'II'), ('III', 'III'), ('IV', 'IV'), ('V', 'V'), ('VI', 'VI'), ('VII', 'VII'), ('VIII', 'VIII'), ('IX', 'IX'), ('X', 'X')], max_length=5, null=True, verbose_name='Номер омонима'),
        ),
        migrations.AlterField(
            model_name='dictionaryentry',
            name='part',
            field=models.CharField(blank=True, choices=[('', ''), ('NOMEN', 'имя'), ('VERBUM', 'глагол'), ('союз', 'союз'), ('межд.', 'междометие'), ('нареч.', 'наречие'), ('част.', 'частица')], max_length=16, null=True, verbose_name='Часть речи'),
        ),
        migrations.AlterField(
            model_name='eaftext',
            name='storage',
            field=models.FileField(max_length=256, upload_to='eaf', verbose_name='Файл EAF'),
        ),
        migrations.AlterField(
            model_name='genre',
            name='name_en',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Name (English)'),
        ),
        migrations.AlterField(
            model_name='genre',
            name='name_ru',
            field=models.CharField(max_length=128, verbose_name='Name (Russian)'),
        ),
        migrations.AlterField(
            model_name='glossedtext',
            name='storage',
            field=models.FileField(blank=True, max_length=256, upload_to='texts', verbose_name='Файл'),
        ),
        migrations.AlterField(
            model_name='text',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='texts', to='khakas.TextAuthor', verbose_name='Author'),
        ),
        migrations.AlterField(
            model_name='text',
            name='city',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='Publication city'),
        ),
        migrations.AlterField(
            model_name='text',
            name='creation_time',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='Time of writing'),
        ),
        migrations.AlterField(
            model_name='text',
            name='genre',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='texts', to='khakas.Genre', verbose_name='Genre'),
        ),
        migrations.AlterField(
            model_name='text',
            name='n_phrases',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Phrases'),
        ),
        migrations.AlterField(
            model_name='text',
            name='n_word_tokens',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Word tokens'),
        ),
        migrations.AlterField(
            model_name='text',
            name='title',
            field=models.CharField(max_length=500, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='text',
            name='translation_city',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='Translation publication city'),
        ),
        migrations.AlterField(
            model_name='text',
            name='translation_title',
            field=models.CharField(blank=True, max_length=500, null=True, verbose_name='Translation title'),
        ),
        migrations.AlterField(
            model_name='text',
            name='translation_year',
            field=models.PositiveIntegerField(blank=True, null=True, verbose_name='Translation publication year'),
        ),
        migrations.AlterField(
            model_name='text',
            name='translator',
            field=models.CharField(blank=True, max_length=64, null=True, verbose_name='Translator'),
        ),
        migrations.AlterField(
            model_name='text',
            name='year',
            field=models.IntegerField(blank=True, null=True, verbose_name='Publication year'),
        ),
        migrations.AlterField(
            model_name='textauthor',
            name='birth_place',
            field=models.TextField(blank=True, null=True, verbose_name='Birth place'),
        ),
        migrations.AlterField(
            model_name='textauthor',
            name='birth_year',
            field=models.IntegerField(blank=True, null=True, verbose_name='Birth year'),
        ),
        migrations.AlterField(
            model_name='textauthor',
            name='first_name',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='First name'),
        ),
        migrations.AlterField(
            model_name='textauthor',
            name='last_name',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Last name'),
        ),
        migrations.AlterField(
            model_name='textauthor',
            name='name',
            field=models.CharField(max_length=100, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='textparagraph',
            name='number',
            field=models.PositiveIntegerField(verbose_name='Paragraph number within text'),
        ),
        migrations.AlterField(
            model_name='textparagraph',
            name='text',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='paragraphs', to='khakas.Text', verbose_name='Text'),
        ),
        migrations.AlterField(
            model_name='textsentence',
            name='number',
            field=models.PositiveIntegerField(verbose_name='Sentence number within paragraph'),
        ),
        migrations.AlterField(
            model_name='textsentence',
            name='paragraph',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sentences', to='khakas.TextParagraph', verbose_name='Paragraph'),
        ),
        migrations.AlterField(
            model_name='textsentence',
            name='translation_en',
            field=models.TextField(blank=True, null=True, verbose_name='English translation'),
        ),
        migrations.AlterField(
            model_name='textsentence',
            name='translation_ru',
            field=models.TextField(blank=True, null=True, verbose_name='Russian translation'),
        ),
        migrations.AlterField(
            model_name='textword',
            name='clause',
            field=models.PositiveIntegerField(verbose_name='Clause'),
        ),
        migrations.AlterField(
            model_name='textword',
            name='confirmed_grammar',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='khakas.WordGrammar', verbose_name='Confirmed grammar info'),
        ),
        migrations.AlterField(
            model_name='textword',
            name='leftpunct',
            field=models.CharField(blank=True, max_length=8, null=True, verbose_name='Left punctuation'),
        ),
        migrations.AlterField(
            model_name='textword',
            name='number',
            field=models.PositiveIntegerField(verbose_name='Word number within sentence'),
        ),
        migrations.AlterField(
            model_name='textword',
            name='rightpunct',
            field=models.CharField(blank=True, max_length=8, null=True, verbose_name='Right punctuation'),
        ),
        migrations.AlterField(
            model_name='textword',
            name='sentence',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='words', to='khakas.TextSentence', verbose_name='Sentence'),
        ),
        migrations.AlterField(
            model_name='textword',
            name='wordform',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='khakas.Wordform', verbose_name='Wordform'),
        ),
        migrations.AlterField(
            model_name='wordfeature',
            name='affix',
            field=models.CharField(max_length=32, verbose_name='Affix'),
        ),
        migrations.AlterField(
            model_name='wordfeature',
            name='feature',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='khakas.AffixFeature', verbose_name='Feature'),
        ),
        migrations.AlterField(
            model_name='wordform',
            name='wordform',
            field=models.CharField(max_length=256, unique=True, verbose_name='Wordform'),
        ),
        migrations.AlterField(
            model_name='wordgrammar',
            name='auto',
            field=models.BooleanField(verbose_name='Automatic parse result'),
        ),
        migrations.AlterField(
            model_name='wordgrammar',
            name='key',
            field=models.CharField(db_index=True, max_length=512, verbose_name='Index key'),
        ),
        migrations.AlterField(
            model_name='wordgrammar',
            name='lexeme',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='khakas.DictionaryEntry', verbose_name='Lexeme'),
        ),
        migrations.AlterField(
            model_name='wordgrammar',
            name='raw_semgloss',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Semantics (if missing in dictionary)'),
        ),
        migrations.AlterField(
            model_name='wordgrammar',
            name='raw_stem',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Stem (if missing in dictionary)'),
        ),
        migrations.AlterField(
            model_name='wordgrammar',
            name='wordform',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='grammars', to='khakas.Wordform', verbose_name='Wordform'),
        ),
        migrations.AlterField(
            model_name='wordgrammarfeature',
            name='position',
            field=models.PositiveIntegerField(verbose_name='Position within wordform'),
        ),
        migrations.AlterField(
            model_name='wordgrammarfeature',
            name='word_feature',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='khakas.WordFeature', verbose_name='Affix and its feature'),
        ),
        migrations.AlterField(
            model_name='wordgrammarfeature',
            name='word_grammar',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='features', to='khakas.WordGrammar', verbose_name='Wordform grammar'),
        ),
    ]
