from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('khakas', '0032_TextSentence_reference_unlimited_length'),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            database_operations=[migrations.RunSQL("""
                ALTER TABLE "khakas_textsentence"
                    ADD COLUMN "paragraph_number" integer DEFAULT 1 NOT NULL CHECK ("paragraph_number" >= 0),
                    ADD COLUMN "text_id" integer DEFAULT 1 NOT NULL CONSTRAINT "khakas_textsentence_text_id_94fde963_fk_khakas_text_id" REFERENCES "khakas_text"("id") DEFERRABLE INITIALLY DEFERRED;
                SET CONSTRAINTS "khakas_textsentence_text_id_94fde963_fk_khakas_text_id" IMMEDIATE;
                UPDATE khakas_textsentence s
                    SET paragraph_number = p.number, text_id = p.text_id
                    FROM khakas_textparagraph p
                    WHERE s.paragraph_id = p.id;
                ALTER TABLE "khakas_textsentence"
                    ALTER COLUMN "paragraph_number" DROP DEFAULT,
                    ALTER COLUMN "text_id" DROP DEFAULT;
                CREATE INDEX "khakas_textsentence_text_id_94fde963" ON "khakas_textsentence" ("text_id");
            """)],
            state_operations=[
                migrations.AddField(
                    model_name='textsentence',
                    name='paragraph_number',
                    field=models.PositiveIntegerField(default=1, verbose_name='Paragraph number within text'),
                    preserve_default=False,
                ),
                migrations.AddField(
                    model_name='textsentence',
                    name='text',
                    field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='sentences', to='khakas.text', verbose_name='Text'),
                    preserve_default=False,
                ),
            ],
        ),
    ]
