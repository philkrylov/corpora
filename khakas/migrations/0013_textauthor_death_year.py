# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2019-09-25 16:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('khakas', '0012_textauthor_birth_place'),
    ]

    operations = [
        migrations.AddField(
            model_name='textauthor',
            name='death_year',
            field=models.IntegerField(blank=True, null=True, verbose_name='Death year'),
        ),
    ]
