# Generated by Django 3.2.13 on 2022-05-28 15:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('khakas', '0035_alter_wordfeature_unique_together'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wordgrammarfeature',
            name='position',
            field=models.IntegerField(db_index=True, verbose_name='Position within wordform'),
        ),
    ]
