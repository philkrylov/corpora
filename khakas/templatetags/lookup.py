import ujson as json

from django import template

register = template.Library()


@register.filter
def lookup(l, a):
    print(l)
    print(a)
    for v in l:
        print(v)
    return next((v for v in l if v['id'] == a), None)


@register.filter
def getattr(d, a):
    return d[a]


@register.filter
def dumps(v):
    return json.dumps(v, ensure_ascii=False)
