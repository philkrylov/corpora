import fnmatch
import os

from django import template
from django.conf import settings

from background_task.models import Task


register = template.Library()


@register.simple_tag
def background_tasks_count():
    return Task.objects.count()
    #return len(fnmatch.filter(os.listdir(os.path.join(settings.BASE_DIR, 'tasks_dir')),
    #                          'uwsgi_spoolfile_*'))
