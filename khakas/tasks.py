import concurrent.futures
from functools import partial
from itertools import chain
import logging

from background_task import background

from django import db
from django.conf import settings
from django.db import transaction
from django.utils import timezone
from requests_futures.sessions import FuturesSession

from suddenly import parse, parse_async, normalize  # noqa: E402
from corpus.models import (  # noqa: E402
    AffixFeature,
    DictionaryEntry,
    WordFeature,
    WordGrammar,
    WordGrammarFeature,
    Wordform,
)

ASYNC_PARSE = True
logger = logging.getLogger('khakas.tasks')  # Can't use __name__ in spooler
analyses_buffer = []


def flush_analysis(data, wordform):
    for grammar in WordGrammar.objects.filter(wordform=wordform,
                                              auto=True):
        grammar.delete()

    wordform.n_auto_grammars = 0
    for homonym in data:
        try:
            entry = DictionaryEntry.objects.get(id=homonym['id'])
        except Exception as e:
            logger.warning("DictionaryEntry %s (%s): %s",
                           homonym['headword'], homonym['id'], e)
            continue

        affixes = homonym['affixes'].split('-')
        signatures = homonym['form'].rsplit('-', maxsplit=len(affixes) - 1)
        if 'prefixes' in homonym:
            prefixes = homonym['prefixes'].split('-')[:-1]
            prefix_signatures = homonym['prefixform'].split('-')[:-1]
        else:
            prefixes = []
            prefix_signatures = []

        key = str(entry)
        for i in range(-1, -len(prefixes) - 1, -1):
            if prefixes[i]:
                key += '+%d:%s:%s' % (i, prefixes[i],
                                      prefix_signatures[i].upper())
        for i in range(len(affixes)):
            if affixes[i]:
                key += '+%d:%s.%s' % (i, affixes[i], signatures[i].upper())

        grammar, created = WordGrammar.objects \
            .get_or_create(wordform=wordform,
                           auto=True,
                           lexeme=entry,
                           key=key,
                           defaults={'weight': 0})
        if created:
            for i, affix, signature in chain(
                (
                    (i, prefixes[i], prefix_signatures[i])
                    for i in range(-1, -len(prefixes) - 1, -1)
                    if prefixes[i]
                ),
                (
                    (i, affixes[i - 1], signatures[i - 1])
                    for i in range(1, len(affixes) + 1)
                    if affixes[i - 1]
                ),
            ):
                feature, _ = AffixFeature.objects \
                    .get_or_create(signature__iexact=signature,
                                   defaults={'signature': signature})
                word_feature, _ = WordFeature.objects \
                    .get_or_create(affix=affix,
                                   feature=feature)
                WordGrammarFeature.objects \
                    .create(word_grammar=grammar,
                            position=i,
                            word_feature=word_feature)
                grammar.weight += 1
            grammar.save(update_fields=['weight'])
        wordform.n_auto_grammars += 1
    wordform.parsed = timezone.now()
    wordform.save(update_fields=('parsed', 'n_auto_grammars'))


@transaction.atomic
def flush_analyses(buffer):
    for data, wordform in buffer:
        flush_analysis(data, wordform)
    logger.debug("%d wordforms flushed", len(buffer))
    buffer[:] = []


def store_analyses(*args, data=None, wordform=None, counter=None, **kwargs):
    analyses_buffer.append((data, wordform))

    counter[0] += 1
    if counter[0] % 100 == 0:
        logger.debug("%d wordforms parsed", counter[0])

    if len(analyses_buffer) == 100:
        flush_analyses(analyses_buffer)


def store_analyses_async(future, *args, wordform=None, counter=None, **kwargs):
    store_analyses(*args, data=future.result().data, wordform=wordform,
                   counter=counter, **kwargs)


def do_reparse(ids, do_store, session=None):
    futures = []
    for wordform in Wordform.objects.filter(pk__in=ids):
        normalized_wordform = normalize(wordform.wordform)
        if ASYNC_PARSE:
            future = parse_async(settings.KHAKAS_PARSER_URL,
                                 normalized_wordform, session)
            future.add_done_callback(partial(do_store,
                                             wordform=wordform))
            futures.append(future)
        else:
            do_store(data=parse(settings.KHAKAS_PARSER_URL,
                                normalized_wordform),
                     wordform=wordform)
    if ASYNC_PARSE:
        concurrent.futures.wait(futures)
    logger.debug("%d wordforms in buffer", len(analyses_buffer))
    if analyses_buffer:
        flush_analyses(analyses_buffer)


@background()
def reparse(arguments):
    ids = arguments['ids']
    logger.debug("==== reparse task: %d wordforms to parse", len(ids))
    shared_counter = [0]
    do_store = partial(store_analyses_async if ASYNC_PARSE else store_analyses,
                       counter=shared_counter)

    db.connections.close_all()
    if ASYNC_PARSE:
        with FuturesSession() as session:
            do_reparse(ids, do_store, session)
    else:
        do_reparse(ids, do_store)
    logger.debug("==== %d wordforms parsed", shared_counter[0])
