import re

from django.conf import settings

from corpus.tokenizer import split_punct
from suddenly import normalize, parse


cache = {}


def is_alphabetic(word):
    return bool(re.search(r'(?i)[а-яӱңғёӧӌі]', word))


def is_parseable(s):
    if s not in cache:
        l, w, r = split_punct(s)
        cache[s] = bool(parse(settings.KHAKAS_PARSER_URL, normalize(w)))
    return cache[s]
