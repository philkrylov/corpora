from django import db
from django.contrib import admin, auth, flatpages, messages

from background_task.admin import TaskAdmin, CompletedTaskAdmin
from background_task.models import CompletedTask, Task

from corpora.admin import CorpusAdminSite

from khakas.models import GlossedText, AudioText, EafText
from corpus.admin import (
    AffixFeatureAdmin,
    DictionaryEntryAdmin,
    OrderedAdmin,
    PersonAdmin,
    PublicationAdmin,
    TextAdmin,
    TextWordAdmin,
    WordFeatureAdmin,
    WordformAdmin,
)
from corpus.models import DictionaryEntry, \
        AffixFeature, \
        Dialect, Genre, Person, Place, Publication, \
        Text, TextWord, \
        WordFeature, Wordform


@admin.action(description='Reparse selected wordforms')
def reparse(modeladmin, request, queryset):
    ids = list(queryset.values_list('id', flat=True))
    db.connections.close_all()
    from . import tasks
    n = 0
    while ids:
        tasks.reparse(dict(ids=ids[:1000]))
        ids = ids[1000:]
        n += 1
    messages.add_message(request, messages.SUCCESS,
                         '%d reparse tasks enqueued.' % n)


class WordformKindFilter(admin.SimpleListFilter):
    title = 'Состав словоформы'
    parameter_name = 'wordform_kind'

    def lookups(self, request, model_admin):
        return (
            ('alpha', 'Alphabetic'),
            ('lower', 'Lower case'),
            ('hyphen', 'Contains hyphen'),
            ('space', 'Contains space'),
            ('disharmonic', 'Broken harmony'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'alpha':
            return queryset.filter(wordform__regex=r'(?i)^[а-яӱңғёӧӌі]')
        if self.value() == 'hyphen':
            return queryset.filter(wordform__regex=r'-')
        if self.value() == 'space':
            return queryset.filter(wordform__regex=r'[ _]')
        if self.value() == 'lower':
            return queryset.filter(wordform__regex=r'^[а-яӱңғёӧӱӌі]')
        if self.value() == 'disharmonic':
            return queryset.filter(
                wordform__regex=r'[аоуы][гк]|[еіӧӱ][хғ]|рга$|рғе$'
            )


class KhakasWordformAdmin(WordformAdmin):
    actions = [reparse]
    list_filter = (WordformAdmin.list_filter[:3] +
                   [WordformKindFilter] +
                   WordformAdmin.list_filter[3:])


admin_site = CorpusAdminSite(name='corpusadmin')
admin_site.register(auth.models.Group, auth.admin.GroupAdmin)
admin_site.register(auth.models.User, auth.admin.UserAdmin)
admin_site.register(admin.models.LogEntry)
admin_site.register(GlossedText)
admin_site.register(EafText)
admin_site.register(AudioText)
admin_site.register(DictionaryEntry, DictionaryEntryAdmin)
admin_site.register(Genre, OrderedAdmin)
admin_site.register(Dialect, OrderedAdmin)
admin_site.register(Place, OrderedAdmin)
admin_site.register(Person, PersonAdmin)
admin_site.register(Publication, PublicationAdmin)
admin_site.register(Text, TextAdmin)
admin_site.register(TextWord, TextWordAdmin)
admin_site.register(Wordform, KhakasWordformAdmin)
admin_site.register(AffixFeature, AffixFeatureAdmin)
admin_site.register(WordFeature, WordFeatureAdmin)
admin_site.register(flatpages.models.FlatPage)
admin_site.register(CompletedTask, CompletedTaskAdmin)
admin_site.register(Task, TaskAdmin)
