# -*- coding: utf-8 -*-

import os

from django.db import models


class GlossedText(models.Model):
    title = models.CharField('Название', max_length=256)
    genre = models.CharField('Жанр', max_length=256)
    author = models.CharField('Автор', max_length=256)
    creation_date = models.CharField('Время написания', max_length=256)
    quant_features = models.CharField('Количественные характеристики',
                                      max_length=256)
    source_biblio = models.CharField(
        'Библиографические данные источника',
        max_length=256,
    )
    translator = models.CharField('Переводчик', max_length=256, blank=True)
    translation_biblio = models.CharField(
        'Библиографические данные перевода',
        max_length=256,
        blank=True,
    )
    storage = models.FileField('Файл', upload_to='texts', max_length=256,
                               blank=True)

    def __str__(self): return self.title


class AudioText(models.Model):
    title = models.CharField('Название', max_length=256)
    text_storage = models.FileField('Текст', upload_to='audio',
                                    max_length=256, blank=True)
    audio_storage = models.FileField('Audio', upload_to='audio',
                                     max_length=256, blank=True)

    def text_extension(self):
        name, extension = os.path.splitext(self.text_storage.name)
        return extension

    def audio_extension(self):
        name, extension = os.path.splitext(self.audio_storage.name)
        return extension

    def __str__(self): return self.title


class EafText(models.Model):
    title_kh = models.CharField(u'Название (по-хакасски)', max_length=256)
    title_ru = models.CharField(u'Название (по-русски)', max_length=256)
    storage = models.FileField(u'Файл EAF', upload_to='eaf', max_length=256)

    def __str__(self): return '%s : %s' % (self.title_kh, self.title_ru)

    class Meta:
        ordering = ['id']
