# -*- coding: utf-8 -*-

import logging
from functools import lru_cache
from urllib.parse import urlencode
from urllib.request import Request, urlopen


logger = logging.getLogger('khakas.suddenly')

TRANS_DICT = {
    '\u04C0': '\u0406',
    '\u04CF': '\u0456',
    'a': 'а',
    'c': 'с',
    'e': 'е',
    'i': '\u0456',
    'ӊ': 'ң',
    'o': 'о',
    'p': 'р',
    'x': 'х',
    'y': 'у',
    'ö': '\u04E7',
    'ÿ': '\u04F1',
    'ҷ': 'ӌ',
    ' ': '_'
}

TRANS_TABLE = {ord(k): v for k, v in TRANS_DICT.items()}


@lru_cache(maxsize=2048)
def normalize(s, lower=True):
    if lower:
        return str(s).lower().translate(TRANS_TABLE)
    return str(s).translate(TRANS_TABLE)


def dictify_suddenly_trace(data):
    data = data.decode('utf8')
    # logger.debug("%s", data)
    homonyms = []
    i = data.find('FOUND STEM:')
    while i >= 0:
        homonym = {}
        data = data[i + 12:]
        i = data.find('\n')
        parts = data[:i].split(' ')
        homonym = {'form': parts[0], 'affixes': parts[1]}
        if len(parts) == 4:
            homonym['prefixform'], homonym['prefixes'] = parts[2:]
        data = data[i + 1:]
        homonym['POS'] = data[:1]
        i = data.find('\n')
        dict_info = data[2:i]
        j = dict_info.find(' ‛')
        homonym['headword'] = dict_info[:j]
        k = dict_info.find('’')
        homonym['meaning'] = dict_info[j + 2:k]
        stem_and_id = dict_info[k + 2:].strip().rsplit(' ', 1)
        if len(stem_and_id) > 1:
            homonym['stem'] = stem_and_id[0].strip()
            homonym['id'] = int(stem_and_id[1])
        else:
            homonym['id'] = int(stem_and_id[0])
        homonyms.append(homonym)
        i = data.find('FOUND STEM:')
    return homonyms


def parse(parser_url, s):
    params = {'parse': s.encode('utf8')}
    query_url = '?'.join((parser_url, urlencode(params)))
    data = urlopen(Request(query_url)).read()
    return dictify_suddenly_trace(data)


def parse_async(parser_url, s, session):
    def postprocessor(r, *args, **kwargs):
        try:
            r.data = dictify_suddenly_trace(r.content)
        except Exception as e:
            raise Exception("{}: {}\n{}"
                            .format(s, r.content.decode('utf8'), e))

    params = {'parse': s.encode('utf8')}
    return session.get(parser_url,
                       params=params,
                       hooks={'response': postprocessor})
