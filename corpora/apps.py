from django.contrib.admin.apps import AdminConfig


class CorpusAdminConfig(AdminConfig):
    default_site = 'corpora.admin.CorpusAdminSite'


__all__ = ['CorpusAdminConfig']
