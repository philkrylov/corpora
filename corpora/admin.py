from django.contrib import admin, messages


class CorpusAdminSite(admin.AdminSite):
    site_header = 'Khakas Corpus administration'
    site_title = 'Khakas Corpus administration'
