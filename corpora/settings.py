"""
Django settings for corpora project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

ALLOWED_HOSTS = [
    'khakas.altaica.ru',
]


# Application definition

INSTALLED_APPS = (
    #'django.contrib.admin',
    #'django.contrib.admin.apps.SimpleAdminConfig',
    'corpora.apps.CorpusAdminConfig',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django_pdb',
    'django.contrib.sessions',
    'django.contrib.messages',
    # 'werkzeug_debugger_runserver',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'clear_cache',
    'admin_auto_filters',
    'rest_framework',
    'background_task',
    'corpus',
    'suddenly',
    'khakas',
    #'debug_toolbar',
    'advanced_filters',
    'drf_spectacular',
)

MIDDLEWARE = (
    #'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'django_pdb.middleware.PdbMiddleware',
)

ROOT_URLCONF = 'corpora.urls'

WSGI_APPLICATION = 'corpora.wsgi.application'


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
        'LOCATION': 'my_cache_table',
    },
    'local': {
        'BACKEND': 'lrucache_backend.LRUObjectCache',
        'TIMEOUT': 600,
        'OPTIONS': {
            'MAX_ENTRIES': 1000
        },
    },
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

SITE_ID = 1

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates/'),
            # insert your TEMPLATE_DIRS here
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
        },
    },
]


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'formatters': {
        'verbose': {
            'format': ('{levelname} {asctime} {module} {process:d} {thread:d}'
                       ' {message}'),
            'style': '{',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            # 'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'corpus': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
            # 'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'khakas': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
            # 'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
    },
}

DATA_UPLOAD_MAX_NUMBER_FIELDS = 20000

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'drf_orjson_renderer.renderers.ORJSONRenderer',
    ),
    'DEFAULT_SCHEMA_CLASS': 'drf_spectacular.openapi.AutoSchema',
}

SPECTACULAR_SETTINGS = {
    'TITLE': 'Language Corpus API',
    'DESCRIPTION': 'Language Corpus API',
    'VERSION': '1.0.0',
    'SERVE_INCLUDE_SCHEMA': True,
    # OTHER SETTINGS
}

CORPUS_LANGUAGE_MODULE = 'corpus'  # should be changed in a real implementation

KHAKAS_PARSER_URL = 'https://khakas.altaica.ru/suddenly/'

from .local_settings import *
