from django.conf.urls import url
from django.contrib import admin
from django.contrib.flatpages.views import flatpage
from django.urls import include, path
from django.views.decorators.cache import cache_page

from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView

#import debug_toolbar

from corpus.views import (
        DictionaryEntryDetailView,
        TextsView,
        CorpusView,
        PersonView,
        SentenceView,
        TextView,
)
from khakas.admin import admin_site
from khakas.views import (
        EafDetailView,
        EafView,
        AudioTextsView,
        TextDetailView,
)


urlpatterns = [
    # Examples:
    # url(r'^$', 'corpora.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', admin_site.urls),
    #path('__debug__/', include(debug_toolbar.urls)),
    url(r'^$', flatpage, {'url': '/'}, name='home'),
    url(r'^advanced_filters/', include('advanced_filters.urls')),

    url(r'^api/schema/$', SpectacularAPIView.as_view(), name='schema'),
    url(r'^api/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    url(r'^api/schema/redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),

    url(r'^api/sentence/$', cache_page(60 * 10)(SentenceView.as_view()),
        name='api_sentence'),
    url(r'^api/text/$', TextView.as_view(), name='api_text'),
    url(r'^corpus/', CorpusView.as_view(), name='corpus'),
    url(r'^dict/(?P<pk>\d+)/$', DictionaryEntryDetailView.as_view(),
        name='dict_detail'),
    url(r'^persons/$', PersonView.as_view(), name='persons'),
    url(r'^texts/(?P<pk>\d+)/$', TextDetailView.as_view(), name='text_detail'),
    url(r'^texts/$', TextsView.as_view(), name='texts'),
    url(r'^eaf/(?P<pk>\d+)/$',
        cache_page(60 * 60 * 24)(EafDetailView.as_view()), name='eaf_detail'),
    url(r'^eaf_texts/$', EafView.as_view(), name='eaf'),
    url(r'^dialects/$', AudioTextsView.as_view(), name='audio_texts'),
]
