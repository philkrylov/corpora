#!/usr/bin/env python
import os
import logging
import signal

import bjoern

import corpora.wsgi

workers = []


def sigterm_handler(sig, frame):
    for _ in workers:
        os.kill(_, signal.SIGINT)


def runner(api_module, host, port, n_workers=1):
    try:
        port = int(os.environ['PORT'])
    except KeyError:
        pass
    logging.getLogger(__name__).info('%d workers listening on %s:%d',
                                     n_workers, host, port)
    wsgi_app = corpora.wsgi.application
    if n_workers == 1:
        bjoern.run(wsgi_app, host, port, reuse_port=True)
    else:
        original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        for _ in range(n_workers):
            pid = os.fork()
            if pid:
                workers.append(pid)
            else:
                bjoern.run(wsgi_app, host, port, reuse_port=True)
                os._exit(0)
        signal.signal(signal.SIGINT, original_sigint_handler)
        signal.signal(signal.SIGTERM, sigterm_handler)

        try:
            for _ in range(n_workers):
                os.wait()
        except KeyboardInterrupt:
            for _ in workers:
                os.kill(_, signal.SIGINT)


if __name__ == '__main__':
    runner(globals(), '127.0.0.1', 8016, n_workers=4)
